CREATE OR REPLACE PACKAGE BODY EDW_LOAD_STAGE.PKG_EDW_BRITE_BILL_STG
AS
  CO_FAILURE         CONSTANT VARCHAR2(1)  := 'F';
  CO_SUCCESS         CONSTANT VARCHAR2(1)  := 'S';
  CO_SUCCESS_MESSAGE CONSTANT VARCHAR2(50) := 'TABLE SUCCESSFULLY LOADED';
  CO_PROCEDURE_NAME  CONSTANT VARCHAR2(30) := 'PKG_EDW_BRITE_BILL_STG';
  V_DATE             DATE                  :=SYSDATE;
  V_START_DATE       DATE;
  V_ROW_INSERTED     NUMBER;
  V_STATUS           NUMBER;
V_ERR_MSG          VARCHAR2(4000);
  CO_TRG_SCHEMA_NAME CONSTANT VARCHAR2(30) := 'EDW_LOAD_STAGE';
  V_TABLE_NAME       VARCHAR2(32);
PROCEDURE PR_BRITE_BILL_RECONTRACT_STG 
    (
      P_RETURN_CODE OUT NUMBER
    )
  IS
  V_Part_name varchar2(100);
  v_sql varchar2(4000);
  v_start_date date;
  V_TABLE_NAME varchar2(100);
  V_ROW_INSERTED number;
  V_part_name_2 varchar2(100);

  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'BRITE_BILL_RECONT_STG_FINAL';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE EDW_LOAD_STAGE.BRITE_BILL_RECONTRACT_STG';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE EDW_LOAD_STAGE.BRITE_BILL_RECONT_STG_FINAL';
                select 'P_'||to_char(max(EDW_REPORT_DATE),'YYYY_MM_DD') into V_Part_name  from DM_OWNER_STR_DL.F_DAILY_LIVE_SERVICE;
    select 'P_'||to_char(max(EDW_REPORT_DATE),'YYYY_MM_DD') into V_Part_name_2 from DM_OWNER_STR_DL.F_DAILY_VISION_SERVICE;
    --TV Starts for RCW
  begin 
  v_sql:= 'INSERT /*+ APPEND */
     INTO EDW_LOAD_STAGE.BRITE_BILL_RECONTRACT_STG (
        BILL_ACCNT_KEY ,
                                ASSET_NUMBER,
        BILL_TV_CONTRACT_END_DATE ,
        IS_IN_TV_RCW 
      )  
          select BILL_ACCNT_KEY,ASSET_NUMBER,edw_date,flag_r from( select /*+parallel(ast,8)*/ast.BILL_ACCNT_KEY,AST.ASSET_NUMBER,dm_dt.edw_date,lf.con_lifecycle_desc,AST.PRODUCT_KEY,case when con_lifecycle_desc in
          (''Re-contract window'',''Out of Contract 1-3'',
''Out of Contract 4+'') then ''TRUE'' else ''FALSE'' end flag_r,
row_number() over(partition by ast.BILL_ACCNT_KEY order by case when con_lifecycle_desc  in (''Re-contract window'',''Out of Contract 1-3'',
''Out of Contract 4+'') then 1 else 0 end desc,dm_dt.edw_date desc) Re_num
from DM_OWNER_STR_DL.F_DAILY_LIVE_SERVICE partition('||v_Part_name||') ast 
inner join  DM_OWNER_STR_DL.F_DAILY_VISION_SERVICE partition('||v_Part_name_2||') asv on asv.asset_number=ast.asset_number 
INNER JOIN  DM_OWNER_STR_DL.DM_DATE DM_DT ON DM_DT.EDW_DATE_KEY=NVL(ASv.CONTRACT_END_DT_KEY,-77) 
lEFT JOIN EDW_REF_OWNER.X_EDW_CON_LIFECYCLE_LKP lf on lf.con_lifecycle_key=ast.CONTR_LIFECYCLE_KEY
WHERE ast.ASSET_TYPE=''TP''
AND AST.PRODUCT_KEY in  (select EDW_PRODUCT_KEY from dm_owner_str_dl.dm_product where curr_product_code in(''S0146404''))) where re_num=1
';
EXECUTE IMMEDIATE V_sql;
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
                Exception when no_data_found then
                V_ERR_MSG := 'No Data Available for BBS0145868Recontract';
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);

                end;
---TV end for RCW

--BB starts for RCW
                begin 
  v_sql:= 
'INSERT  /*+ APPEND */
     INTO EDW_LOAD_STAGE.BRITE_BILL_RECONTRACT_STG (
        BILL_ACCNT_KEY ,
                                ASSET_NUMBER,---ADDED BTR-115327/BTRCE-216867/DSS-35180/RETAILEDW-24103/EDW - Customer Fact creation and enablement on current feed to BB
        BB_CONTRACT_END_DATE ,
        IS_IN_BB_RCW 
      )  
          select BILL_ACCNT_KEY,ASSET_NUMBER,edw_date,flag_r from(select /*+parallel(ast,8)*/ast.BILL_ACCNT_KEY,AST.ASSET_NUMBER,dm_dt.edw_date,lf.con_lifecycle_desc,AST.PRODUCT_KEY,case when con_lifecycle_desc in
          (''Re-contract window'',''Out of Contract 1-3'',
''Out of Contract 4+'') then ''TRUE'' else ''FALSE'' end flag_r,
row_number() over(partition by ast.BILL_ACCNT_KEY order by case when con_lifecycle_desc  in (''Re-contract window'',''Out of Contract 1-3'',
''Out of Contract 4+'') then 1 else 0 end desc,dm_dt.edw_date desc) Re_num
from DM_OWNER_STR_DL.F_DAILY_LIVE_SERVICE partition('||v_Part_name||') ast 
INNER JOIN  DM_OWNER_STR_DL.DM_DATE DM_DT ON DM_DT.EDW_DATE_KEY=NVL(ASt.CONTRACT_END_DT_KEY,-77) 
lEFT JOIN EDW_REF_OWNER.X_EDW_CON_LIFECYCLE_LKP lf on lf.con_lifecycle_key=ast.CONTR_LIFECYCLE_KEY
WHERE ast.ASSET_TYPE=''TP''
AND AST.PRODUCT_KEY in  (select EDW_PRODUCT_KEY from dm_owner_str_dl.dm_product where curr_product_code in(''S0145868''))) where re_num=1';
EXECUTE IMMEDIATE V_sql;
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
                Exception when no_data_found then
                V_ERR_MSG := 'No Data Available for BB Recontract';
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);

                end;
    
    --BB ends for RCW
    --Mobile starts for RCW
                begin 
                 select 'P_'||to_char(max(EDW_REPORT_DATE),'YYYY_MM_DD') into V_Part_name  from DM_OWNER_STR_DL.F_DAILY_MOB_LIVE_SERVICE;
  v_sql:= 'INSERT
      /*+ APPEND */
    INTO EDW_LOAD_STAGE.BRITE_BILL_RECONTRACT_STG
      (
        BILL_ACCNT_KEY ,
                                ASSET_NUMBER,---ADDED BTR-115327/BTRCE-216867/DSS-35180/RETAILEDW-24103/EDW - Customer Fact creation and enablement on current feed to BB
        MOBILE_CONTRACT_END_DATE ,
        IS_IN_MOBILE_RCW 
      )
  select BILL_ACCNT_NUM,GSM_ASSET_NUMBER,edw_date,flag_r from(select /*+parallel(ast,8)*/ast.BILL_ACCNT_NUM,AST.GSM_ASSET_NUMBER,dm_dt.edw_date,lf.con_lifecycle_desc,case when 
  con_lifecycle_desc in (''Re-contract window'',''Out of Contract 1-3'',
''Out of Contract 4+'') then ''TRUE'' else ''FALSE'' end flag_r,
row_number() over(partition by ast.BILL_ACCNT_NUM order by case when con_lifecycle_desc in (''Re-contract window'',''Out of Contract 1-3'',
''Out of Contract 4+'') then 1 else 0 end desc,dm_dt.edw_date desc) Re_num
from DM_OWNER_STR_DL.F_DAILY_MOB_LIVE_SERVICE partition('||v_Part_name||') ast 
INNER JOIN  DM_OWNER_STR_DL.DM_DATE DM_DT ON DM_DT.EDW_DATE_KEY=NVL(AST.CONTRACT_END_DT_KEY,-77) 
lEFT JOIN EDW_REF_OWNER.X_EDW_CON_LIFECYCLE_LKP lf on lf.con_lifecycle_key=ast.mob_contr_lifecycle_key
) where re_num=1';
EXECUTE IMMEDIATE V_sql;
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
                Exception when no_data_found then
                V_ERR_MSG := 'No Data Available for Mobile Recontract';
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);

                end;       
--Mobile ends for RCW
---Start  BTR-115327/BTRCE-216867/DSS-35180/RETAILEDW-24103/EDW - Customer Fact creation and enablement on current feed to BB
--PSTN starts for RCW
                begin 
  v_sql:= 
'INSERT  /*+ APPEND */
     INTO EDW_LOAD_STAGE.BRITE_BILL_RECONTRACT_STG (
        BILL_ACCNT_KEY ,
                                ASSET_NUMBER,
                                PSTN_CONTRACT_END_DATE,
        IS_IN_PSTN_RCW 
      )  
          select BILL_ACCNT_KEY,ASSET_NUMBER,edw_date,flag_r from
                                    (select /*+parallel(ast,8)*/ast.BILL_ACCNT_KEY,ASSET_NUMBER,dm_dt.edw_date,lf.con_lifecycle_desc,AST.PRODUCT_KEY,case when con_lifecycle_desc in(''Re-contract window'',''Out of Contract 1-3'',''Out of Contract 4+'') then ''TRUE'' else ''FALSE'' end flag_r,
                                  
row_number() over(partition by ast.BILL_ACCNT_KEY order by case when con_lifecycle_desc  in (''Re-contract window'',''Out of Contract 1-3'',''Out of Contract 4+'') then 1 else 0 end desc,dm_dt.edw_date desc) Re_num

from DM_OWNER_STR_DL.F_DAILY_LIVE_SERVICE partition('||v_Part_name||') ast 
INNER JOIN  DM_OWNER_STR_DL.DM_DATE DM_DT ON DM_DT.EDW_DATE_KEY=NVL(ASt.CONTRACT_END_DT_KEY,-77) 
lEFT JOIN EDW_REF_OWNER.X_EDW_CON_LIFECYCLE_LKP lf on lf.con_lifecycle_key=ast.CONTR_LIFECYCLE_KEY
WHERE ast.ASSET_TYPE=''TP''
AND AST.PRODUCT_KEY in  (select EDW_PRODUCT_KEY from dm_owner_str_dl.dm_product where curr_product_code in(''S0131489''))) where re_num=1';
EXECUTE IMMEDIATE V_sql;
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
                Exception when no_data_found then
                V_ERR_MSG := 'No Data Available for PSTN Recontract';
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);

                end;
    
--PSTN ends for RCW


--SPORT starts for RCW
                begin 
  v_sql:= 
'INSERT  /*+ APPEND */
     INTO EDW_LOAD_STAGE.BRITE_BILL_RECONTRACT_STG (
        BILL_ACCNT_KEY ,
                                ASSET_NUMBER,
                                SPORT_CONTRACT_END_DATE,
        IS_IN_SPORT_RCW 
      )  
          select BILL_ACCNT_KEY,ASSET_NUMBER,edw_date,flag_r from(select /*+parallel(ast,8)*/ast.BILL_ACCNT_KEY,ast.ASSET_NUMBER,dm_dt.edw_date,lf.con_lifecycle_desc,AST.PRODUCT_KEY,case when con_lifecycle_desc in
          (''Re-contract window'',''Out of Contract 1-3'',
''Out of Contract 4+'') then ''TRUE'' else ''FALSE'' end flag_r,
row_number() over(partition by ast.BILL_ACCNT_KEY order by case when con_lifecycle_desc  in (''Re-contract window'',''Out of Contract 1-3'',
''Out of Contract 4+'') then 1 else 0 end desc,dm_dt.edw_date desc) Re_num
from DM_OWNER_STR_DL.F_DAILY_LIVE_SERVICE partition('||v_Part_name||') ast 
INNER JOIN  DM_OWNER_STR_DL.DM_DATE DM_DT ON DM_DT.EDW_DATE_KEY=NVL(ASt.CONTRACT_END_DT_KEY,-77) 
lEFT JOIN EDW_REF_OWNER.X_EDW_CON_LIFECYCLE_LKP lf on lf.con_lifecycle_key=ast.CONTR_LIFECYCLE_KEY
WHERE ast.ASSET_TYPE=''TP''
AND AST.PRODUCT_KEY in  (select EDW_PRODUCT_KEY from dm_owner_str_dl.dm_product where curr_product_code in(''S0312138''))) where re_num=1';
EXECUTE IMMEDIATE V_sql;
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
                Exception when no_data_found then
                V_ERR_MSG := 'No Data Available for SPORT Recontract';
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);

                end;
    
--SPORT ends for RCW

--END  BTR-115327/BTRCE-216867/DSS-35180/RETAILEDW-24103/EDW - Customer Fact creation and enablement on current feed to BB
--final stage starts
begin
v_sql:='insert into  EDW_LOAD_STAGE.BRITE_BILL_RECONT_STG_FINAL select /*+ parallel(8) */ BILL_ACCNT_key ,max(BILL_TV_CONTRACT_END_DATE ) BILL_TV_CONTRACT_END_DATE,max(IS_IN_TV_RCW) IS_IN_TV_RCW,max(BB_CONTRACT_END_DATE) BB_CONTRACT_END_DATE,max(IS_IN_BB_RCW) IS_IN_BB_RCW
,max(MOBILE_CONTRACT_END_DATE) MOBILE_CONTRACT_END_DATE,max(IS_IN_MOBILE_RCW) IS_IN_MOBILE_RCW,max(PSTN_CONTRACT_END_DATE ) PSTN_CONTRACT_END_DATE,max(IS_IN_PSTN_RCW) IS_IN_PSTN_RCW,max(SPORT_CONTRACT_END_DATE ) SPORT_CONTRACT_END_DATE,max(IS_IN_SPORT_RCW) IS_IN_SPORT_RCW,ASSET_NUMBER
from EDW_LOAD_STAGE.BRITE_BILL_RECONTRACT_STG group by BILL_ACCNT_KEY,ASSET_NUMBER';

EXECUTE IMMEDIATE V_sql;
  V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
                Exception when no_data_found then
                V_ERR_MSG := 'No Data Available for Insert into Final stage';
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);

end;
--final merge
   PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  PROCEDURE PR_LOG_AUDIT(
      PROCEDURE_NAME     IN CUSTOMER_AGGREGATED_CACHE.A_TAB_RTD_AUDIT.PROCEDURE_NAME%TYPE,
      TRG_SCHEMA         IN CUSTOMER_AGGREGATED_CACHE.A_TAB_RTD_AUDIT.TRG_SCHEMA%TYPE,
      TRG_TABLE          IN CUSTOMER_AGGREGATED_CACHE.A_TAB_RTD_AUDIT.TRG_TABLE%TYPE,
      STATUS             IN CUSTOMER_AGGREGATED_CACHE.A_TAB_RTD_AUDIT.STATUS%TYPE,
      MESSAGE            IN CUSTOMER_AGGREGATED_CACHE.A_TAB_RTD_AUDIT.MESSAGE%TYPE,
      ROW_COUNT          IN CUSTOMER_AGGREGATED_CACHE.A_TAB_RTD_AUDIT.ROW_COUNT%TYPE,
      PROCESS_START_DATE IN CUSTOMER_AGGREGATED_CACHE.A_TAB_RTD_AUDIT.PROCESS_START_DATE%TYPE)
  IS
    /**************************************************************
    *
    * PROGRAM NAME:   PR_LOG_AUDIT
    *
    * AUTHOR:         Jayanandha
    * CREATION DATE : 07-June-2018
    * VERSION :       1.0
    * DESCRIPTION :   INSERTS AUDIT INFORMATION INTO  A_TAB_RTD_AUDIT TABLE, WITH ROW_COUNT, START_TIME AND OTHER DETAILS.
    * MODIFICATIONS:  A HISTORY OF CHANGES TO THE PROGRAM
    ********************************************************************/
    P_INSTANCE_ID PLS_INTEGER;
    P_SESSION_ID PLS_INTEGER;
  BEGIN
    SELECT SYS_CONTEXT('USERENV', 'INSTANCE') INSTANCE_ID,
      SYS_CONTEXT('USERENV', 'SID') SESSION_ID
    INTO P_INSTANCE_ID,
      P_SESSION_ID
    FROM DUAL;
    --
    INSERT
    INTO CUSTOMER_AGGREGATED_CACHE.A_TAB_RTD_AUDIT
      (
        ID,
        PROCEDURE_NAME,
        TRG_SCHEMA,
        TRG_TABLE,
        STATUS,
        MESSAGE,
        ROW_COUNT,
        PROCESS_START_DATE,
        PROCESS_END_DATE,
        INSTANCE_ID,
        SESSION_ID
      )
      VALUES
      (
        CUSTOMER_AGGREGATED_CACHE.A_TAB_RTD_AUDIT_SEQ.NEXTVAL,
        PROCEDURE_NAME,
        TRG_SCHEMA,
        TRG_TABLE,
        STATUS,
        MESSAGE,
        ROW_COUNT,
        PROCESS_START_DATE,
        SYSDATE,
        P_INSTANCE_ID,
        P_SESSION_ID
      );
    --
    COMMIT;
    --
  EXCEPTION
  WHEN OTHERS THEN
    RAISE;
  END PR_LOG_AUDIT;
  PROCEDURE PR_W_EDW_BRITE_BILL_CONS_MAP
    (
      P_RETURN_CODE OUT NUMBER
    )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'W_EDW_BRITE_BILL_CONS_MAP';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_EDW_BRITE_BILL_CONS_MAP';
    INSERT
      /*+ APPEND */
    INTO W_EDW_BRITE_BILL_CONS_MAP
      (
        "MAP_BILLING_ACCOUNT" ,
        "IS_FIBRE_ELIGIBLE" ,
        "IS_TV_ELIGIBLE" ,
        "HAS_COPPER_BROADBAND" ,
        "IS_CARRIER_PRE_SELECT" ,
        "HAS_CALLER_DISPLAY" ,
        "IS_BT_TV" ,
        "IS_BT_EMPLOYEE" ,
        "HAS_BROADBAND_PACKAGE" ,
        "HAS_TV_STARTER_PLAN" ,
        "HAS_TV_ENTERTAINMENT_PLAN" ,
        "HAS_TV_MAX_PLAN" ,
        "HAS_ULTRA_FAST_FIBRE_PLUS" ,
        "HAS_ULTRA_FAST_FIBRE_2_PLUS" ,
        "HAS_SUPER_FAST_FIBRE_2_UNL" ,
        "HAS_BROADBAND_UNLIMITED" ,
        "HAS_SUPER_FAST_FIBRE_1" ,
        "HAS_SUPER_FAST_FIBRE_LITE"
      )
    SELECT
      /*+ PARALLEL(CONS_MAP,8)    PARALLEL(ELI,8)*/
      map_biLling_account,
      MAX (
      CASE
        WHEN CONS_MAP.BB_PROD_STATUS ='L'
        AND CONS_MAP.BB_FIBRE_FLAG   ='Y'
        THEN 'True'
        WHEN CONS_MAP.BB_PROD_STATUS  ='L'
        AND ELI.PRODUCT_ELIGIBILTY_FLG='Y'
        AND UPPER(ELI_LKP.EDW_DESC) LIKE '%INFINITY%'
        THEN 'True'
        ELSE 'False'
      END ) IS_FIBRE_ELIGIBLE,
      MAX (
      CASE
        WHEN CONS_MAP.BB_PROD_STATUS   ='L'
        AND ELI.PRODUCT_ELIGIBILTY_FLG ='Y'
        AND ELI_LKP.EDW_DESC           ='TV VoD'
        THEN 'True'
        WHEN CONS_MAP.BB_PROD_STATUS ='L'
        AND AVIL.DSLMAX_SPEED        > 3.5
        AND AVIL.RA_RAG             IN ('G','A')
        THEN 'True'
        ELSE 'False'
      END ) IS_TV_ELIGIBLE,
      MAX (
      CASE
        WHEN CONS_MAP.BB_PROD_STATUS ='L'
        AND CONS_MAP.BB_FIBRE_FLAG   ='N'
        THEN 'True'
        ELSE 'False'
      END ) HAS_COPPER_BROADBAND,
      MAX(
      CASE
        WHEN (CONS_MAP.VOICE_PROM_CURR_BASIS NOT IN ('RESOFF', 'BTBASIC')
        OR CONS_MAP.VOICE_PROM_CURR_BASIS        IS NULL)
        AND CONS_MAP.VOICE_SERVICE_STATUS         = 'L'
        AND CONS_MAP.BT_RETAIL_ALL_CALLS_FLG      = 'N'
        THEN 'True'
        ELSE 'False'
      END) IS_CARRIER_PRE_SELECT,
      MAX(
      CASE
        WHEN NVL(CALLER_DISPLAY_CURR,'N')<>'N'
        THEN 'True'
        ELSE 'False'
      END) HAS_CALLER_DISPLAY,
      MAX(
      CASE
        WHEN BT_VISION_FLG ='A'
        THEN 'True'
        ELSE 'False'
      END ) IS_BT_TV,
      MAX(
      CASE
        --WHEN CONS_MAP.VOICE_SERVICE_STATUS = 'L'
        --AND pstn_prom.PROMOTION_TYPE       ='Staff'
        --THEN 'True'
        WHEN CONS_MAP.BB_PROD_STATUS   ='L'
        AND CONS_MAP.BB_PROMOTION_TYPE ='Employee Broadband'
        THEN 'True'
        WHEN CONS_MAP.BT_VISION_FLG          ='A'
        AND VISION_PROM.promotion_group_lvl1 = 'Employee'
        THEN 'True'
        ELSE 'False'
      END) IS_BT_EMPLOYEE,
      MAX(
      CASE
        WHEN BB_PROD_STATUS ='L'
        AND BUNDLE_IND      ='Y'
        THEN 'True'
        ELSE 'False'
      END ) HAS_BROADBAND_PACKAGE,
      MAX(
      CASE
        WHEN BT_VISION_FLG            ='A'
        AND vision_pgl1.promotion_plan='Starter'
        THEN 'True'
        ELSE 'False'
      END ) HAS_TV_STARTER_PLAN,
      MAX(
      CASE
        WHEN BT_VISION_FLG            ='A'
        AND vision_pgl1.promotion_plan='Entertainment'
        THEN 'True'
        ELSE 'False'
      END ) HAS_TV_ENTERTAINMENT_PLAN,
      MAX(
      CASE
        WHEN BT_VISION_FLG            ='A'
        AND vision_pgl1.promotion_plan='Max'
        THEN 'True'
        ELSE 'False'
      END ) HAS_TV_MAX_PLAN,
      MAX(
      CASE
        WHEN BB_PROD_STATUS       ='L'
        AND bb_pgl1.promotion_plan='Ultrafast Fibre Plus'
        THEN 'True'
        ELSE 'False'
      END ) HAS_ULTRA_FAST_FIBRE_PLUS,
      MAX(
      CASE
        WHEN BB_PROD_STATUS       ='L'
        AND bb_pgl1.promotion_plan='Ultrafast Fibre 2 Plus'
        THEN 'True'
        ELSE 'False'
      END ) HAS_ULTRA_FAST_FIBRE_2_PLUS,
      MAX(
      CASE
        WHEN BB_PROD_STATUS       ='L'
        AND bb_pgl1.promotion_plan='Superfast Fibre 2 Unlimited'
        THEN 'True'
        ELSE 'False'
      END ) HAS_SUPER_FAST_FIBRE_2_UNL,
      MAX(
      CASE
        WHEN BB_PROD_STATUS       ='L'
        AND bb_pgl1.promotion_plan='Broadband Unlimited'
        THEN 'True'
        ELSE 'False'
      END ) HAS_BROADBAND_UNLIMITED,
      MAX(
      CASE
        WHEN BB_PROD_STATUS       ='L'
        AND bb_pgl1.promotion_plan='Superfast Fibre 1'
        THEN 'True'
        ELSE 'False'
      END ) HAS_SUPER_FAST_FIBRE_1,
      MAX(
      CASE
        WHEN BB_PROD_STATUS       ='L'
        AND bb_pgl1.promotion_plan='Superfast Fibre Lite'
        THEN 'True'
        ELSE 'False'
      END ) HAS_SUPER_FAST_FIBRE_LITE
    FROM MAP_OWNER.L_EDW_MAP_CONSOLIDATED CONS_MAP,
      INT_OWNER_IL.L_EDW_SVCE_PT_ELIGIBLE_PRODS ELI,
      EDW_REF_OWNER.X_EDW_PRODUCT_TYPE_LKP ELI_LKP,
      MAP_OWNER.L_EDW_BB_AVAILABILITY_MASTER AVIL,
      dm_owner_str_dl.dm_promotion VISION_PROM,
      dm_owner_str_dl.dm_promotion BB_PROM,
      dm_owner_str_dl.dm_promotion pstn_prom,
      EDW_REF_OWNER.X_EDW_PROM_GRP_LVL1_MAP vision_pgl1,
      EDW_REF_OWNER.X_EDW_PROM_GRP_LVL1_MAP bb_pgl1
    WHERE CONS_MAP.map_biLling_account                         IS NOT NULL
    AND NVL (CONS_MAP.MAP_SERVICE_PT , CONS_MAP.BB_SERVICE_PT ) = ELI.SERVICE_POINT_ID (+)
    AND ELI.PRODUCT_ELIGIBILTY_FLG(+)                           = 'Y'
    AND ELI_LKP.EDW_LKP_KEY(+)                                  = ELI.ELIGIBLE_PROD_TYPE_KEY
    AND CONS_MAP.MAP_SERVICE_ID                                 =AVIL.SERVICE_NUM(+)
    AND CONS_MAP.VISION_PROMOTION_CURR                          =VISION_PROM.CURR_PRODUCT_CODE(+)
    AND VISION_PROM.promotion_product_line(+)                   ='VISION'
    AND VISION_PROM.promotion_product_line                      =vision_pgl1.promotion_product_line(+)
    AND VISION_PROM.promotion_group_lvl1                        =vision_pgl1.promotion_group_lvl1(+)
    AND CONS_MAP.BB_PROMOTION_CURR                              =BB_PROM.CURR_PRODUCT_CODE(+)
    AND BB_PROM.promotion_product_line(+)                       ='BB'
    AND BB_PROM.promotion_product_line                          =bb_pgl1.promotion_product_line(+)
    AND BB_PROM.promotion_group_lvl1                            =bb_pgl1.promotion_group_lvl1(+)
    AND CONS_MAP.VOICE_PROMOTION_CURR                           =pstn_prom.CURR_PRODUCT_CODE(+)
    AND pstn_prom.promotion_product_line(+)                     ='PSTN'
    GROUP BY map_biLling_account;
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  PROCEDURE PR_W_EDW_BRITE_BILL_CONSENT(
      P_RETURN_CODE OUT NUMBER )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'W_EDW_BRITE_BILL_CONSENT';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_EDW_BRITE_BILL_CONSENT';
    INSERT
      /*+ APPEND */
    INTO W_EDW_BRITE_BILL_CONSENT
      (
        "OU_NUM" ,
        "IS_CONSENT_TO_MARKET"
      )
    SELECT
      /*+ parallel(CONS,8) */
      ou_num,
      MIN(
      CASE
        WHEN CONS.consent_ind IN ('C','X')
        THEN 'True'
        ELSE 'False'
      END) IS_CONSENT_TO_MARKET
    FROM sim_owner.bac_address_rel REL,
      int_owner_il.l_edw_consent CONS,
      oneview_owner.s_org_ext partition (p_billing) s_org_ext
    WHERE CONS.address_key         =REL.address_key
    AND REL.pr_address_for_bac_flg ='Y'
    AND REL.deleted_flg            ='N'
    AND CONS.CONSENT_TYPE          ='DNM'
    AND CONS.CONTACT_POINT_TYPE    = 'AD'
    AND REL.bill_accnt_key         =s_org_ext.loc
    AND s_org_ext.cust_stat_cd     ='Active'
                and  s_org_ext.ou_type_cd='Consumer'
    AND ou_num                    IS NOT NULL
    GROUP BY ou_num;
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  PROCEDURE PR_W_EDW_BRITE_BILL_CUST_SEG(
      P_RETURN_CODE OUT NUMBER )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'W_EDW_BRITE_BILL_CUST_SEG';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_EDW_BRITE_BILL_CUST_SEG';
    INSERT
      /*+ APPEND */
    INTO W_EDW_BRITE_BILL_CUST_SEG
      (
        "CONSUMER_KEY" ,
        "HAS_SPORT_ON_APP" ,
        "HAS_SPORT_ON_APP_ONLY" ,
        "HAS_SPORT_ON_TV_FIBRE" ,
        "HAS_SPORT_ON_TV_COPPER"
      )
    SELECT CONSUMER_KEY,
      MAX(
      CASE
        WHEN L_EDW_MAP_CUST_SEG.TOTAL_BT_SPORT_DIGITAL > 0
        THEN 'True'
        ELSE 'False'
      END ) HAS_SPORT_ON_APP,
      MAX(
      CASE
        WHEN L_EDW_MAP_CUST_SEG.TOTAL_BT_SPORT_DIGITAL   > 0
        AND L_EDW_MAP_CUST_SEG.TOTAL_BT_SPORT_VISION_MCC = 0
        AND L_EDW_MAP_CUST_SEG.TOTAL_BT_SPORT_VISION_DTT = 0
        AND L_EDW_MAP_CUST_SEG.TOTAL_BT_SPORT_SKY        = 0
        THEN 'True'
        ELSE 'False'
      END ) HAS_SPORT_ON_APP_ONLY,
      MAX(
      CASE
        WHEN L_EDW_MAP_CUST_SEG.TOTAL_BT_SPORT_VISION_MCC >0
        AND BB_SERV_LINE.fibre_flg                        ='Y'
        THEN 'True'
        ELSE 'False'
      END ) HAS_SPORT_ON_TV_FIBRE,
      MAX(
      CASE
        WHEN L_EDW_MAP_CUST_SEG.TOTAL_BT_SPORT_VISION_MCC >0
        AND BB_SERV_LINE.fibre_flg                        ='N'
        THEN 'True'
        ELSE 'False'
      END ) HAS_SPORT_ON_TV_COPPER
    FROM MAP_OWNER.L_EDW_MAP_CUST_SEG L_EDW_MAP_CUST_SEG,
      (SELECT DISTINCT bb_line_type_cd,
        fibre_flg
      FROM EDW_REF_OWNER.X_EDW_MAP_BB_SERV_LINE
      ) BB_SERV_LINE
    WHERE CONSUMER_KEY                IS NOT NULL
    AND BB_SERV_LINE.BB_LINE_TYPE_CD(+)=L_EDW_MAP_CUST_SEG.BB_LINE_TYPE_CD
    GROUP BY CONSUMER_KEY;
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  PROCEDURE PR_W_EDW_BRITE_BILL_INCIDENT(
      P_RETURN_CODE OUT NUMBER )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'W_EDW_BRITE_BILL_INCIDENT';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_EDW_BRITE_BILL_INCIDENT';
    INSERT
      /*+ APPEND */
    INTO W_EDW_BRITE_BILL_INCIDENT
      (
        "CUST_KEY" ,
        "COUNT_OPEN_FAULT" ,
        "HAS_OPEN_COMPLAINT"
      )
    SELECT
      /*+ parallel(l_edw_incident,4) */
      cust_key,
      SUM(
      CASE
        WHEN source_incident_type='Fault'
        AND edw_desc             ='Open'
        THEN 1
        ELSE 0
      END) COUNT_OPEN_FAULT,
      MAX(
      CASE
        WHEN source_incident_type='Complaint'
        AND edw_desc             ='Open'
        THEN 'True'
        ELSE 'False'
      END) HAS_OPEN_COMPLAINT
    FROM int_owner_il.l_edw_incident l_edw_incident,
      edw_ref_owner.X_EDW_LOOKUP_incident
    WHERE repeat_flg      ='N'
    AND current_status_key=edw_lkp_key
    AND EDW_LKP_ATTRIBUTE ='Incident_status_key'
    AND cust_key         IS NOT NULL
    GROUP BY cust_key;
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  PROCEDURE PR_W_EDW_BRITE_BILL_MOB(
      P_RETURN_CODE OUT NUMBER )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'W_EDW_BRITE_BILL_MOB';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_EDW_BRITE_BILL_MOB';
    INSERT
      /*+ APPEND */
    INTO W_EDW_BRITE_BILL_MOB
      (
        "BILL_ACCNT_NUM" ,
        "HAS_BT_MOBILE_SIM_ONLY" ,
        "HAS_BT_MOBILE_FAMILY_SIM" ,
        "HAS_BT_MOBILE_SMARTPHONE" ,
        "HAS_MOBILE_ROAMING_RESTRICTION"
      )
    SELECT BILL_ACCNT_NUM,
      MAX(
      CASE
        WHEN promotion_group_lvl1 IN ('SIM Only','SIM Only-Employee')
        THEN 'True'
        ELSE 'False'
      END) HAS_BT_MOBILE_SIM_ONLY,
      MAX(
      CASE
        WHEN promotion_group_lvl1='Multi SIM only'
        THEN 'True'
        ELSE 'False'
      END) HAS_BT_MOBILE_FAMILY_SIM,
      MAX(
      CASE
        WHEN UPPER(promotion_group_lvl1) LIKE '%HANDSET%'
        THEN 'True'
        ELSE 'False'
      END) HAS_BT_MOBILE_SMARTPHONE,
      MAX(
      CASE
        WHEN ROAMING_AVLBL_FLG='N'
        THEN 'True'
        ELSE 'False'
      END ) HAS_MOBILE_ROAMING_RESTRICTION
    FROM MAP_OWNER.L_EDW_MOBILE_MAP,
      dm_owner_str_dl.dm_promotion
    WHERE MOB_GSM_SERVICE_STATUS='L'
    AND mob_promotion_key       =edw_product_key
    AND promotion_product_line  ='BT Mobile'
    AND BILL_ACCNT_NUM         IS NOT NULL
    GROUP BY BILL_ACCNT_NUM;
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  PROCEDURE PR_W_EDW_BRITE_BILL_SPORTS(
      P_RETURN_CODE OUT NUMBER )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'W_EDW_BRITE_BILL_SPORTS';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_EDW_BRITE_BILL_SPORTS';
    INSERT
      /*+ APPEND */
    INTO W_EDW_BRITE_BILL_SPORTS
      (
        "BILL_ACCNT_NUM" ,
        "HAS_BT_SPORT_PAID_FOR"
      )
    SELECT bill_accnt_num,
      MAX(
      CASE
        WHEN PAYING_FOR_BT_SPORT_FLG='Y'
        THEN 'True'
        ELSE 'False'
      END) HAS_BT_SPORT_PAID_FOR
    FROM map_owner.l_edw_bt_sport_map
    WHERE bts_service_status='L'
    AND BILL_ACCNT_NUM     IS NOT NULL
    GROUP BY bill_accnt_num;
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  PROCEDURE PR_W_EDW_BRITE_BILL_GIFTED_BB(
      P_RETURN_CODE OUT NUMBER )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'W_EDW_BRITE_BILL_GIFTED_BB';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_EDW_BRITE_BILL_GIFTED_BB';
    INSERT /*+ APPEND */
    INTO W_EDW_BRITE_BILL_GIFTED_BB
      ( BILL_ACCNT_NUM, cnt
      )
    SELECT
      /*+ full(DM_ASSET_ATTRIBUTE_OV) parallel(DM_ASSET_ATTRIBUTE_OV,8) */
      BILL_ACCNT_NUM,
      COUNT(1) cnt
    FROM DM_OWNER_STR_DL.DM_ASSET_OV DM_ASSET_OV,
      DM_OWNER_STR_DL.DM_ASSET_ATTRIBUTE_OV DM_ASSET_ATTRIBUTE_OV
    WHERE DM_ASSET_OV.ACTIVE_FLG                   ='Y' -- partitioned hit
    AND DM_ASSET_OV.DELETED_FLG                    ='N'
   AND DM_ASSET_OV.ASSET_NUMBER                   =DM_ASSET_ATTRIBUTE_OV.ASSET_NUMBER
    AND DM_ASSET_ATTRIBUTE_OV.ASSET_ATTRIBUTE_NAME ='Usage'
    AND DM_ASSET_ATTRIBUTE_OV.SOURCE_SYSTEM        ='OV'
    AND DM_ASSET_ATTRIBUTE_OV.ACTIVE_FLG           ='Y'
    AND DM_ASSET_ATTRIBUTE_OV.ATTRIBUTE_VALUE      ='Gifted'
    AND BILL_ACCNT_NUM                            IS NOT NULL
    GROUP BY BILL_ACCNT_NUM;
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  PROCEDURE PR_W_EDW_BRITE_BILL_DISCOUNT(
      P_RETURN_CODE OUT NUMBER )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'W_EDW_BRITE_BILL_DISCOUNT';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_EDW_BRITE_BILL_DISCOUNT';
    INSERT
      /*+ APPEND */
    INTO W_EDW_BRITE_BILL_DISCOUNT
      (
        BILL_ACCNT_NUM,
        min_dis_end_date,
        HAS_REALISED_BT_PLUS_BENEFIT
      )
    SELECT
      /*+ full(DM_ASSET_OV) parallel(DM_ASSET_OV,8) */
      BILL_ACCNT_NUM,
      MIN(
      CASE
       WHEN DM_PRODUCT.PRODUCT_FAMILY ='Discount'
        THEN dm_date.edw_date
      END) min_dis_end_date,
      MAX(
      CASE
        WHEN DM_PRODUCT.curr_product_code='S0356171'
        THEN 'True'
        ELSE 'False'
      END) HAS_REALISED_BT_PLUS_BENEFIT
    FROM DM_OWNER_STR_DL.DM_ASSET_OV DM_ASSET_OV,
      DM_OWNER_STR_DL.DM_PRODUCT DM_PRODUCT,
      DM_OWNER_STR_DL.dm_date dm_date
    WHERE DM_ASSET_OV.ACTIVE_FLG        ='Y'
    AND DM_ASSET_OV.DELETED_FLG         ='N'
    AND DM_ASSET_OV.EDW_PRODUCT_KEY     =DM_PRODUCT.EDW_PRODUCT_KEY
    AND (DM_PRODUCT.PRODUCT_FAMILY      ='Discount'
    OR curr_product_code                ='S0356171')
    AND DM_ASSET_OV.edw_Asset_End_Dt_key=dm_date.edw_date_key
    AND BILL_ACCNT_NUM                 IS NOT NULL
    GROUP BY BILL_ACCNT_NUM;
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  PROCEDURE PR_W_EDW_BRITE_BILL_USR_PROF(
      P_RETURN_CODE OUT NUMBER )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'W_EDW_BRITE_BILL_USR_PROFILE';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_EDW_BRITE_BILL_USR_PROFILE';
    INSERT
      /*+ APPEND */
    INTO W_EDW_BRITE_BILL_USR_PROFILE
      (
        bill_accnt_num,
        DATE_OF_LAST_LOGIN,
        HAS_BT_ID_BEEN_ACTIVATED
      )
    SELECT bupr.bill_accnt_num ,
      MAX(up.LAST_LOGIN_DT) DATE_OF_LAST_LOGIN,
      MAX(
      CASE
       WHEN up.PROFILE_TYPE = 'BT_ID'
        THEN 'True'
        ELSE 'False'
      END) HAS_BT_ID_BEEN_ACTIVATED
    FROM int_owner_il.L_EDW_USER_PROFILE up,
      int_owner_il.L_EDW_BAC_USER_PROF_REL bupr,
      sim_owner.bac_consumer_rel bcr
    WHERE up.SOURCE_DELETED_FLG='N'
    AND up.profile_id          = bupr.profile_id
    AND 'GenevaGB'
      || bupr.bill_accnt_num       = bcr.bill_accnt_key
    AND bupr.deleted_flg           = 'N'
    AND bcr.deleted_flg            = 'N'
    AND bcr.PR_BAC_FOR_CONsumER_FLG='Y'
    AND bupr.BILL_ACCNT_NUM       IS NOT NULL
    AND bupr.relationship_type     ='ACHT'
    GROUP BY bupr.bill_accnt_num ;
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  PROCEDURE PR_W_EDW_BRITE_BILL_BILL_REV(
      P_RETURN_CODE OUT NUMBER )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'W_EDW_BRITE_BILL_BILL_REV';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_EDW_BRITE_BILL_BILL_REV';
    INSERT
      /*+ APPEND */
    INTO W_EDW_BRITE_BILL_BILL_REV
      (
        "ACCOUNT_NUM" ,
        "CUST_KEY" ,
        "BILL_DATE" ,
        "NEXT_BILL_DATE" ,
        "FOLLOW_UP_RATING"
      )
    SELECT
      /*+ parallel(a,8) */
      a.account_num,
      MAX(B.cust_key) cust_key,
      MAX(a.bill_date) bill_date,
      MAX(a.next_bill_date) next_bill_date,
      MAX(B.FOLLOW_UP_RATING) FOLLOW_UP_RATING
    FROM int_owner_il.L_EDW_BILL_REVENUE a,
      int_owner_il.L_EDW_bac_x b
    WHERE a.account_num=b.bill_accnt_num
    AND a.account_num IS NOT NULL
    GROUP BY a.account_num;
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  
  ---START  BTR-115327/BTRCE-216867/DSS-35180/RETAILEDW-24103/EDW - Customer Fact creation and enablement on current feed to BB
PROCEDURE PR_CUST_EDW_BRITE_BILL_FACT_1
    (
      P_RETURN_CODE OUT NUMBER
    )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'CUSTOMER_EDW_BRITE_BILL_STG1';
   EXECUTE IMMEDIATE 'TRUNCATE TABLE EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG1';
INSERT /*+PARALLEL(CEB,8)+*/ INTO EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG1 CEB
(CEB.BILL_ACCNT_NUM,
CEB.MARKET_FLAG                               ,   
--CEB.DATE_OF_CONTRACT_START                    ,
CEB.STATUS_OF_BILLING_ACCOUNT                 ,
CEB.CANCELLATION_DIRECT_DEBIT                 ,
CEB.DATE_OF_DIRECT_DEBIT_CANC       ,
CEB.COUNT_COMPLAINTS_3MONTHS                  ,
CEB.COUNT__OPEN_COMPLAINTS    ,
CEB.COUNT_FAULTS_3MONTHS                      ,
CEB.COUNT_ENQUIRIES_3MONTHS                   ,
CEB.HAS_AOT_IN_PROGRESS                       ,
CEB.DATE_OF_OLDEST_BB_SERVICE                 ,
CEB.COUNT_OF_CONTACTS_1M                      ,
CEB.COUNT_OF_CONTACTS_6M                      ,
CEB.IS_CUSTOMER_TENURE                        ,
CEB.COUNT_COMPLAINTS_CURRENT_MONTH            ,
CEB.COUNT_BT_VISIT_CURRENT_MONTH        ,
CEB.DATE_OF_BT_WIFI_LAST_USE                  ,
CEB.HAS_BT_WIFI_ACTIVATED                     ,
CEB.TOTAL_BT_WIFI_USAGE_3MONTHS               ,
CEB.IS_SUB_15_FIBRE_ELIGIBLE                  ,
CEB.IS_TV_UHD_ELIGIBLE                        ,
CEB.IS_GFAST_100_ELIGIBLE                     ,
CEB.IS_GFAST_200_ELIGIBLE                     ,
CEB.DATE_OF_FIBRE_ELIGIBILE_START         ,
CEB.DATE_OF_FIBRE_RFS_INDICATIVE              ,
CEB.IS_BB_FRESHER                             ,
CEB.IS_BT_TV_FRESHER                          ,
CEB.AVERAGE_MOBILE_MINS_LAST_12MTH            ,
CEB.AVERAGE_LOCAL_MINS_LAST_12MTH             ,
CEB.IS_ELIGIBLE_FOR_FTTP                      ,
CEB.IS_MULTICAST_ON_FIBRE_ELIGIBLE            ,
CEB.IS_MULTICAST_ON_COPER_ELIGIBLE         ,
CEB.IS_ULTRAFAST_FTTP_ELIGIBLE                ,
CEB.IS_SUPER_FAST_FIBRE_ELIGIBLE              ,
CEB.HAS_BT_MAILBOX                            ,
CEB.HAS_CREDIT_STATUS                         ,
CEB.IS_MOBILE_4G_ELIGIBLE                     ,
CEB.HAS_BT_CLOUD                              ,
CEB.IS_MY_ANYTIME_CAL_PLN_ELIGIBLE        ,
CEB.IS_ELIGIBLE_ADSL                          ,
CEB.IS_ELIGIBLE_ADSL2                         ,
CEB.HAS_VIEWED_FOOTBALL                       ,
CEB.HAS_VIEWED_CRICKET                        ,
CEB.HAS_VIEWED_BOXING                         ,
CEB.HAS_VIEWED_RUGBY                          ,
CEB.HAS_VIEWED_SPORT                          ,
CEB.HAS_VIEWED_MOTORSPORT                     ,
CEB.HAS_VIEWED_AMC                            ,
CEB.COUNT_BT_SPORT_VIEWS_3M_APP               ,
CEB.COUNT_BT_SPORT_VIEWS_3M                   ,
CEB.HAS_VIRUS_PROTECT                         ,
CEB.HAS_PARENTAL_CONTROL                      ,
CEB.DATE_OF_PARENTAL_ACTIVATION     ,
CEB.TOTAL_MINUTES_CURRENT_MONTH               ,
CEB.TOTAL_MINUTES_LAST_3_MONTHS               ,
CEB.TOTAL_INMINUTE_LAST_3_MONTH         
)
(
SELECT /*+PARALLEL(SADB,8)+*/  DISTINCT
SADB.BILL_ACCNT_NUM,
SADB.MARKET_A_B,
--SADB.CONTRACT_START_DT,
SADB.BAC_STATUS,
SADB.DD_CANCEL_FLG,
SADB.DD_CANCELLATION_DATE,
SADB.COMPLAINT_COUNT_3M,
SADB.COMPLAINT_COUNT_1M_OPEN,
SADB.FAULT_COUNT_3M,
SADB.ENQUIRY_COUNT_3M,
SADB.AOT_OPEN_FLG,
SADB.BB_OLDEST_ST_DT,
SADB.CON_TOT_COUNT_1M,
SADB.CON_TOT_COUNT_6M,
SADB.CUSTOMER_TENURE_MONTHS,
SADB.COMPLAINT_COUNT_1M_CLOSED,
SADB.ONLINE_VISIT_1M,
SADB.WIFI_LAST_USE_DT,
SADB.WIFI_ACTIVE_FLG,
SADB.USG_WIFI_3M,
SADB.ELIG_SUB_15M_FIBRE_FLG,
SADB.ELIG_TV_UHD_FLG,
SADB.ELIG_GFAST_100_FLG,
SADB.ELIG_GFAST_200_FLG,
SADB.ELIG_FTTC_START_DT,
SADB.FTTC_INDICATIVE_RFS_DT,
SADB.BB_FRESHER_FLG,
SADB.TV_FRESHER_FLG,
SADB.USG_AVG_MOBILE_MINUTES_12M,
SADB.USG_AV_LOCAL_MINUTES_12M,
SADB.ELIG_FTTP_FLG,
SADB.ELIG_MULTICAST_FIBRE_FLG,
SADB.ELIG_MULTICAST_COPPER_FLG,
SADB.ELIG_ULTRAFAST_FTTP_FLG,
SADB.ELIG_SF_FIBRE,
SADB.HAS_MAILBOX,
SADB.CREDIT_STATUS_DESC,
SADB.ELIG_MOBILE_4G_FLG,
SADB.CLOUD_USER,
SADB.ELIG_MY_ANYTIME_CALL_PLAN,
SADB.ELIG_ADSL_FLG,
SADB.ELIG_ADSL2_PLUS_FLG,
SADB.FOOTBALL_VIEW_FLG,
SADB.CRICKET_VIEW_FLG,
SADB.BOXING_VIEW_FLG,
SADB.RUGBY_VIEW_FLG,
SADB.SPORT_GENERAL_VIEW_FLG,
SADB.MOTORSPORT_VIEW_FLG,
SADB.AMC_VIEWED_3M_FLG,
SADB.NO_BT_SPORT_APP_VIEWS_3M,
SADB.NO_BT_SPORT_VIEWS_3M,
SADB.VIRUS_PROTECT_ACTIVE,
SADB.PARENTAL_CONTROL_ACTIVE_FLG,
SADB.PARENTAL_CTRL_ACTIVATED_DT,
SADB.TOTAL_MINUTES_M1,
SADB.TOTAL_MINUTES_P1,
SADB.INTL_MINUTES_P1
FROM 
EDW_LOAD_STAGE.W_EDW_BRITE_BILL_BILL_REV CEB,
SMV_OWNER.SVOC_ACCOUNT_DAILY_BT SADB
WHERE SADB.BILL_ACCNT_NUM=CEB.ACCOUNT_NUM
AND CEB.NEXT_BILL_DATE=TRUNC(SYSDATE));



    V_ROW_INSERTED                            := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  
  
  PROCEDURE PR_CUST_EDW_BRITE_BILL_FACT_2
    (
      P_RETURN_CODE OUT NUMBER
    )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'CUSTOMER_EDW_BRITE_BILL_STG2';
  EXECUTE IMMEDIATE 'TRUNCATE TABLE EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG2';
INSERT /*+PARALLEL(CEB,8)+*/ INTO EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG2 CEB
(CEB.BILL_ACCNT_NUM,
CEB.HAS_BROADBAND_HUB                   ,
CEB.HAS_BT_SPORT_BT_TV_HD               ,
CEB.HAS_BT_SPORT_SKY_HD                 ,
CEB.DATE_OF_OLDEST_PSTN_SERVICE         ,
CEB.HAS_TV_SKY_SPORTS                   ,
CEB.EXISTENCE_PREVIOUS_BROADBAND        ,
CEB.HAS_MULTIPLE_BILLING_ACCOUNTS       ,
CEB.IS_PRIMARY_BILLING_ACOUNT           ,
CEB.IS_CUSTOMER_ORGANISATION            ,
CEB.DATE_OF_ACCOUNT_START               ,
CEB.IS_ACCOUNT_TYPE                     ,
CEB.HAS_DEBT_STATUS                     ,
CEB.HAS_BTTV                            ,
CEB.IS_BT_PREMIUM_MAIL_STATUS           ,
CEB.HAS_OPEN_ORDER_FOR_BROADBAND        ,
CEB.HAS_OPEN_ORDER_FOR_BTTV             ,
CEB.HAS_OPEN_ORDER_FOR_BT_SPORT         ,
CEB.HAS_CALL_BARRING                    ,
CEB.HAS_CALL_PROTECT                    ,
CEB.HAS_NOW_TV                          ,
CEB.DATE_OF_OLDEST_BT_TV_SERVICE        ,
CEB.DATE_OF_OLDEST_BT_MOB_SRVICE    ,
CEB.DATE_OF_OLDEST_BT_SPRT_SRVIC     ,
CEB.HAS_BT_TV_APP                       ,
CEB.DATE_OF_DIRECT_DEBIT_START          ,
CEB.IS_BT_SPORT_PLATFORM                ,
CEB.HAS_FILM_TV_BOX_SET                 ,
CEB.HAS_TV_MUSIC_BOLT_ON                ,
CEB.HAS_TV_KIDS_BOLT_ON                 ,
CEB.HAS_TV_KIDS                         ,
CEB.HAS_TV_KIDS_EXTRA                   ,
CEB.HAS_SMART_HUB                       ,
CEB.HAS_SMART_HUB_2                     ,
CEB.HAS_HD_EXTRA                        ,
CEB.HAS_BT_SPORT_HD                     ,
CEB.BT_SPORT_SECOND_BOX
)
(SELECT /*+PARALLEL(SANR,8)+*/  DISTINCT
SANR.BILL_ACCNT_NUM,
SANR.HUB_OWNERSHIP,
SANR.BT_SPORT_BT_TV_HD,
SANR.BT_SPORT_SKY_HD,
SANR.PSTN_OLDEST_ST_DT,
SANR.TV_SKY_SPORTS,
SANR.BB_FLG,
SANR.CSTMR_MULTI_BAC,
SANR.PR_BAC_FOR_CUST_FLG,
SANR.CUSTOMER_ORG,
SANR.ACCOUNT_START_DATE,
SANR.ACCOUNT_TYPE,
SANR.DEBT_STATUS,
SANR.HAS_TV,
SANR.PREMIUM_MAIL_STATUS,
SANR.OPEN_ORDER_BB_FLG,
SANR.OPEN_ORDER_TV_FLG,
SANR.ORD_BT_SPORT_OPEN_FLG,
SANR.CALL_BARRING_ACTIVATED,
SANR.CALL_PROTECT_ACTIVATED,
SANR.NOW_TV_FLG,
SANR.TV_OLDEST_ST_DT,
SANR.MOBILE_OLDEST_ST_DT,
SANR.BT_SPORT_OLDEST_ST_DT,
SANR.TV_APP_FLG,
SANR.DD_START_DT,
SANR.BT_SPORT_PLATFORM,
SANR.FILM_TV_BOX_SETS,
SANR.TV_MUSIC_BOLTON,
SANR.TV_KIDS_BOLTON,
SANR.TV_KIDS,
SANR.TV_KIDS_EXTRA,
SANR.SMART_HUB_FLG,
SANR.SMART_HUB2_FLG,
SANR.HD_EXTRA,
SANR.BT_SPORT_HD,
SANR.BT_SPORT_SECOND_BOX 
FROM 
EDW_LOAD_STAGE.W_EDW_BRITE_BILL_BILL_REV CEB,
SMV_OWNER.SVOC_ACCOUNT_NRT_7D SANR
WHERE SANR.BILL_ACCNT_NUM=CEB.ACCOUNT_NUM
AND SANR.EDW_CREATE_DATE>=trunc(SYSDATE-1)
AND CEB.NEXT_BILL_DATE=TRUNC(SYSDATE));

    V_ROW_INSERTED                            := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  
  PROCEDURE PR_CUST_EDW_BRITE_BILL_FACT_3
    (
      P_RETURN_CODE OUT NUMBER
    )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'CUSTOMER_EDW_BRITE_BILL_STG3';
  EXECUTE IMMEDIATE 'TRUNCATE TABLE EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG3';
INSERT /*+PARALLEL(CEB,8)+*/ INTO EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG3 CEB
(
CEB.BILL_ACCNT_NUM,
CEB.HAS_BUNDLE,
CEB.HAS_LINE_RENTAL_SAVER,
CEB.IS_PSTN_CNTRCT_END,
--CEB.DATE_LINE_RTNL_SAVER_CNTCT_END,
--CEB.DATE_OF_CONTRACT_END,
CEB.IS_TV_PROMOTION,
CEB.IS_PSTN_CALLING_PLAN,
CEB.IS_PSTN_CALLING_PLAN_OFFER,
CEB.DATE_OF_PSTN_OFFER_END,
CEB.DATE_OF_PSTN_OFFER_START,
CEB.IS_BB_TENURE,
CEB.IS_PSTN_TENURE,
CEB.IS_BROADBAND_PROMOTION,
CEB.BROADBAND_SERVICE_LINE_TYPE,
CEB.DATE_MSS_PROMOTION_START,
CEB.DATE_MSS_PROMOTION_END,
CEB.DATE_BT_TV_CONTRACT_START,
CEB.DATE_BT_TV_DISCOUNT_END,
CEB.IS_BT_SPORT_PROMOTION,
CEB.HAS_BT_SPORT_ON_BT_TV_UHD,
CEB.BT_MOBILE_FAMILY_SIM_PROMOTION,
--CEB.DATE_OF_DISCOUNT_START,
CEB.DATE_OF_DISCOUNT_END,
CEB.DATE_VOICE_PROM_DISCOUNT_END,
--CEB.HAS_DYNAMIC_DISCOUNT,
--CEB.IS_DYNAMIC_DISCOUNT_VALUE,
CEB.HAS_MOBILE_ROAMING_AVAILABLE,
CEB.IS_BT_MOBILE_CALL_PLAN,
CEB.HAS_UNLIMITED_BB_PACKAGE,
--CEB.IS_SERVICE_SUB_TYPE,
--CEB.HAS_EARLY_LIFE_FLAG,
--CEB.IS_CONTRACT_TYPE,
--CEB.DATE_OF_SERVICE_START,
--CEB.DATE_OF_SERVICE_END,
CEB.HAS_BT_TV_MOCO,
CEB.HAS_BT_TV_MULTICAST,
CEB.IS_BT_BROADBAND_PROMOTION,
CEB.IS_LANDLINE_PROMOTION,
CEB.IS_BT_TV_PROMOTION,
CEB.IS_BT_MOBILE_PROMOTION,
--CEB.IS_BUNDLE_PROMOTION,
CEB.HAS_BT_TV_HD
)
(SELECT /*+PARALLEL(SSPN,8) +*/ DISTINCT SSPN.BILL_ACCNT_NUM,
MAX(CASE WHEN  SSPN.SERVICE_TYPE in ('BB','PSTN') THEN SSPN.BUNDLE ELSE NULL END)  BUNDLE ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('PSTN') THEN SSPN.LINE_RENTAL_SAVER ELSE NULL END) LINE_RENTAL_SAVER ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('PSTN') THEN SSPN.CONTRACT_STATUS ELSE NULL END) CONTRACT_STATUS ,
--MAX(CASE WHEN SSPN.SERVICE_TYPE in ('PSTN') THEN SSPN.LINE_RTNL_SAVER_CNTCT_END_DT ELSE NULL END) LINE_RTNL_SAVER_CNTCT_END_DT ,
--MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BB','PSTN') THEN SSPN.CONTRACT_END_DT ELSE NULL END) CONTRACT_END_DT ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BTTV') THEN SSPN.CURR_OFFER ELSE NULL END) CURR_OFFER ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('PSTN') THEN SSPN.PROM_CURR_BASIS ELSE NULL END) PROM_CURR_BASIS ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BTTV') THEN SSPN.CURR_OFFER ELSE NULL END) CURR_OFFER ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('PSTN') THEN SSPN.CURR_OFFER_END_DT ELSE NULL END) CURR_OFFER_END_DT ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('PSTN') THEN SSPN.CURR_OFFER_START_DT ELSE NULL END) CURR_OFFER_START_DT ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BB') THEN SSPN.SERVICE_TENURE_MONTH ELSE NULL END) SERVICE_TENURE_MONTH ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('PSTN') THEN SSPN.SERVICE_TENURE_MONTH ELSE NULL END) SERVICE_TENURE_MONTH ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BB') THEN SSPN.CURR_OFFER ELSE NULL END) CURR_OFFER ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BB') THEN SSPN.SERVICE_LINE_TYPE ELSE NULL END) SERVICE_LINE_TYPE ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('Mobile') THEN SSPN.CURR_OFFER_START_DT ELSE NULL END) CURR_OFFER_START_DT ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('Mobile') THEN SSPN.CURR_OFFER_END_DT ELSE NULL END) CURR_OFFER_END_DT ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BTTV') THEN SSPN.CONTRACT_START_DT ELSE NULL END) CONTRACT_START_DT ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BTTV') THEN SSPN.DISCOUNT_PROD_END_DT ELSE NULL END) DISCOUNT_PROD_END_DT ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in  ('Sports') THEN SSPN.CURR_OFFER ELSE NULL END) CURR_OFFER ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in  ('Sports') THEN SSPN.TV_UHD_FLG ELSE NULL END) TV_UHD_FLG ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in  ('Mobile') THEN SSPN.CURR_OFFER ELSE NULL END) CURR_OFFER ,
--MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BB','PSTN') THEN SSPN.DISCOUNT_PROD_START_DT ELSE NULL END) DISCOUNT_PROD_START_DT ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('PSTN') THEN SSPN.DISCOUNT_PROD_END_DT ELSE NULL END) DISCOUNT_PROD_END_DT ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('PSTN') THEN SSPN.DISCOUNT_PROD_END_DT ELSE NULL END) DISCOUNT_PROD_END_DT ,
--MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BB','PSTN') THEN SSPN.DYNM_DISC_PROMO_FLG ELSE NULL END) DYNM_DISC_PROMO_FLG ,
--MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BB','PSTN') THEN SSPN.DYNM_DISC_DELTA_VAL ELSE NULL END) DYNM_DISC_DELTA_VAL ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in  ('Mobile') THEN SSPN.MOB_RM_AVLBL_FLG ELSE NULL END) MOB_RM_AVLBL_FLG ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in  ('Mobile') THEN SSPN.MOB_CALL_PLAN ELSE NULL END) MOB_CALL_PLAN ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BB') THEN SSPN.BB_UNLIMITED_PKG ELSE NULL END) BB_UNLIMITED_PKG ,
--MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BB','PSTN') THEN SSPN.SERVICE_SUB_TYPE ELSE NULL END) SERVICE_SUB_TYPE ,
--MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BB','PSTN') THEN SSPN.EARLY_LIFE_FLG ELSE NULL END) EARLY_LIFE_FLG ,
--MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BB','PSTN') THEN SSPN.contract_type ELSE NULL END) contract_type ,
--MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BB','PSTN') THEN SSPN.SERVICE_START_DT ELSE NULL END) SERVICE_START_DT ,
--MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BB','PSTN') THEN SSPN.SERVICE_END_DT ELSE NULL END) SERVICE_END_DT ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BTTV') THEN SSPN.TV_MOCO_FLG ELSE NULL END) TV_MOCO_FLG ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BTTV') THEN SSPN.TV_MULTICAST_FLG ELSE NULL END) TV_MULTICAST_FLG ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BB') THEN SSPN.CURR_OFFER ELSE NULL END) CURR_OFFER ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('PSTN') THEN SSPN.CURR_OFFER ELSE NULL END) CURR_OFFER ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BTTV') THEN SSPN.CURR_OFFER ELSE NULL END) CURR_OFFER ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('Mobile') THEN SSPN.CURR_OFFER ELSE NULL END) CURR_OFFER ,
--MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BB','PSTN') THEN SSPN.CURR_OFFER ELSE NULL END) CURR_OFFER ,
MAX(CASE WHEN SSPN.SERVICE_TYPE in ('BTTV') THEN SSPN.TV_OMNI_HD_FLG ELSE NULL END) TV_OMNI_HD_FLG 
FROM 
EDW_LOAD_STAGE.W_EDW_BRITE_BILL_BILL_REV CEB,
SMV_OWNER.SVOC_SERVICE_PRODUCT_NRT_7D SSPN,
EDW_LOAD_STAGE.BRITE_BILL_RECONT_STG_FINAL BBR
WHERE SSPN.BILL_ACCNT_NUM=CEB.ACCOUNT_NUM
AND SSPN.BILL_ACCNT_NUM=BBR.BILL_ACCNT_KEY 
AND SSPN.SERVICE_ID=BBR.ASSET_NUMBER
and SSPN.EDW_CREATE_DATE>=trunc(SYSDATE-1)
AND CEB.NEXT_BILL_DATE=TRUNC(SYSDATE)
group by SSPN.bill_accnt_num);


    V_ROW_INSERTED                            := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  
   PROCEDURE PR_CUST_EDW_BRITE_BILL_FACT_4
    (
      P_RETURN_CODE OUT NUMBER
    )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'CUSTOMER_EDW_BRITE_BILL_STG4';
   EXECUTE IMMEDIATE 'TRUNCATE TABLE EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG4';
INSERT /*+PARALLEL(CEB,8)+*/ INTO EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG4 CEB
(CEB.BILL_ACCNT_NUM,
CEB.HAS_TPS_FLAG,
CEB.DATE_OF_TV_VOD,
CEB.IS_BB_AVERAGE_DOWNLOAD_SPEED,
CEB.IS_BT_TV_SERVICE_TYPE,
CEB.IS_BT_TV_STATUS,
CEB.HAS_BT_MOBILE_APP,
CEB.HAS_BT_TV_KIDS_USAGE,
CEB.HAS_BT_TV_SPORT_USAGE,
CEB.HAS_BT_TV_ENTERTAINMENT_USAGE,
CEB.HAS_BT_TV_VOD_USAGE,
CEB.BT_TV_DEVICE_TYPE,
CEB.IS_BT_TV_DEVICE_MODEL,
CEB.IS_BT_TV_DEVICE_MODEL_NAME,
CEB.IS_BT_TV_CONNECTION_STATUS,
CEB.HAS_NETFLIX,
CEB.IS_NETFLIX_PACKAGE
)
(SELECT /*+PARALLEL(SSPN,8)+*/ DISTINCT  SSPD.BILL_ACCNT_KEY ,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('PSTN')   THEN   SSPD.CONSENT_TPS_VAL      ELSE NULL END ) CONSENT_TPS_VAL,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('BTTV')   THEN   SSPD.TV_LATEST_VOD_DT     ELSE NULL END ) TV_LATEST_VOD_DT,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('BB')     THEN   SSPD.AVG_DOWNLOAD_SPEED   ELSE NULL END ) AVG_DOWNLOAD_SPEED,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('BTTV')   THEN   SSPD.TV_SERVICE_TYPE      ELSE NULL END ) TV_SERVICE_TYPE,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('BTTV')   THEN   SSPD.TV_STATUS_KEY        ELSE NULL END ) TV_STATUS_KEY,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('MOBILE') THEN   SSPD.MOBILE_APP_FLG       ELSE NULL END ) MOBILE_APP_FLG,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('BTTV')   THEN   SSPD.USG_KIDS             ELSE NULL END ) USG_KIDS,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('BTTV')   THEN   SSPD.SPORTS_TV_USG        ELSE NULL END ) SPORTS_TV_USG,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('BTTV')   THEN   SSPD.USG_ENTENTERTAINMENT ELSE NULL END ) USG_ENTENTERTAINMENT,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('BTTV')   THEN   SSPD.USG_TV_VOD           ELSE NULL END ) USG_TV_VOD,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('BTTV')   THEN   SSPD.TV_DEVICE_TYPE       ELSE NULL END ) TV_DEVICE_TYPE,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('BTTV')   THEN   SSPD.TV_DEVICE_MODEL      ELSE NULL END ) TV_DEVICE_MODEL,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('BTTV')   THEN   SSPD.TV_DEVICE_MODEL_NAME ELSE NULL END ) TV_DEVICE_MODEL_NAME,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('BTTV')   THEN   SSPD.TV_CONNECTION_STATUS ELSE NULL END ) TV_CONNECTION_STATUS,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('BTTV')   THEN   SSPD.NETFLIX              ELSE NULL END ) NETFLIX,
MAX(CASE WHEN SSPD.SERVICE_TYPE IN ('BTTV')   THEN   SSPD.NETFLIX_PKG          ELSE NULL END ) NETFLIX_PKG
FROM 
EDW_LOAD_STAGE.W_EDW_BRITE_BILL_BILL_REV CEB,
SMV_OWNER.SVOC_SERVICE_PRODUCT_DAILY_BT SSPD,
EDW_LOAD_STAGE.BRITE_BILL_RECONT_STG_FINAL BBR
WHERE SUBSTR(SSPD.BILL_ACCNT_KEY,-10,10)=CEB.ACCOUNT_NUM
AND SUBSTR(SSPD.BILL_ACCNT_KEY,-10,10)=BBR.BILL_ACCNT_KEY 
AND SSPD.SERVICE_ID=BBR.ASSET_NUMBER
AND CEB.NEXT_BILL_DATE=TRUNC(SYSDATE)
GROUP BY SSPD.BILL_ACCNT_KEY);


    V_ROW_INSERTED                            := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  
  
   PROCEDURE PR_CUST_EDW_BRITE_BILL_FACT_5
    (
      P_RETURN_CODE OUT NUMBER
   )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'CUSTOMER_EDW_BRITE_BILL_STG5';
   EXECUTE IMMEDIATE 'TRUNCATE TABLE EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG5';
INSERT /*+PARALLEL(CEB,8)+*/ INTO EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG5 CEB
(
CEB.BILL_ACCNT_NUM                     ,
CEB.TOTAL_AVERAGE_DAY_REVENUE_P1       ,
CEB.TOTAL_AVERAGE_DAY_REVENUE_F        ,
CEB.AVERAGE_LOCAL_NON_GEO_MIN          ,
CEB.AVERAGE_NATIONAL_GEO_MINS_L        ,
CEB.AVERAGE_NATIONAL_NON_GEO_MI        ,
CEB.AVERAGE_TOTAL_CALLS_LAST_12MTH     ,
CEB.AVERAGE_TOTAL_CALL_MINS_LAS        ,
CEB.AVERAGE_DAY_MINS_12MTHS            ,
CEB.TOTAL_EVE_MINUTES_LAST_3_MO        ,
CEB.TOTAL_EVE_WKND_MINUTES_LAST        ,
CEB.AVERAGE_EVE_CALLS_12MTHS           ,
CEB.AVERAGE_EVWKND_MINS_12MTHS         ,
CEB.AVERAGE_INTL_MINS_LAST_12MTH       ,
CEB.AVERAGE_INTL_MOBILE_MINS_LA        ,
CEB.AVERAGE_LOCAL_GEO_MINS_LAST        ,
CEB.AVERAGE_MOBILE_MINS_LAST_12MTH     ,
--CEB.AVERAGE_FIXED_TO_MOBILE_MIN        ,
CEB.TOTAL_UK__MOBILE_MINS_LAST         ,
CEB.TOTAL_LOCAL_GEO_MINS_LAST_3MTH     ,
CEB.TOTAL_LOCAL_NON_GEO_MINS_L         ,
CEB.TOTAL_WEEKEND_MINS_LAST_3MTH       ,
CEB.TOTAL_NATIONAL_NON_GEO_MIN         ,
CEB.TOTAL_NATIONAL_GEO_MINS_LA         ,
CEB.TOTAL_INTERNATIONAL_FREEDO         ,
CEB.TOTAL_INTERNATIONAL_ROW_MI         ,
CEB.TOTAL_INTERNATIONAL__MOBIL         ,
CEB.TOTAL_DAY_TIME_MINS_LAST_3MTHS    
)
(SELECT /*+PARALLEL(LERE,8)+*/ DISTINCT
LERE.BILL_ACCNT_NUM,
LERE.DAY_REV_UK_P1_RTD,
LERE.DAY_REV_FTM_P1_RTD,
LERE.AV_LOCAL_NONGEO_MINUTES_12M,
LERE.AV_NATIONAL_GEO_MINUTES_12M,
LERE.AV_NATIONAL_NONGEO_MINUTES_12M,
LERE.AVG_TOTAL_CALLS_12M,
LERE.AVG_TOTAL_MINUTES_12M,
LERE.AV_DAY_MINS_12M,
LERE.EVE_MINS_P1,
LERE.EVWKEND_MINS_P1,
LERE.AV_EVE_CALLS_12M,
LERE.AV_EVWKEND_MINS_12M,
LERE.AV_INTL_MINS_12M,
LERE.AV_INTL_MOB_MINS_12M,
LERE.AV_LOCAL_GEO_MINUTES_12M,
LERE.AV_MOBILE_MINUTES_12M,
--LERE.SOME VALUE,
LERE.MOBILE_MINUTES_P1,
LERE.LOCAL_GEO_MINUTES_P1,
LERE.LOCAL_NONGEO_MINUTES_P1,
LERE.WEEKEND_MINS_P1,
LERE.NATIONAL_NONGEO_MINUTES_P1,
LERE.NATIONAL_GEO_MINUTES_P1,
LERE.INTL_FREEDOM_MINUTES_P1,
LERE.INTL_ROW_MINUTES_P1,
LERE.INTL_MOB_MINS_P1,
LERE.DAY_MINS_P1
FROM 
EDW_LOAD_STAGE.W_EDW_BRITE_BILL_BILL_REV CEB,
INT_OWNER_IL.L_EDW_REVENUE_ENGINE LERE
WHERE LERE.BILL_ACCNT_NUM=CEB.ACCOUNT_NUM
AND CEB.NEXT_BILL_DATE=TRUNC(SYSDATE));


                                                                                                                                                  
    V_ROW_INSERTED                            := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
  
  
  
 PROCEDURE PR_CUST_EDW_BRITE_BILL_FACT_6
    (
      P_RETURN_CODE OUT NUMBER
    )
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'CUSTOMER_EDW_BRITE_BILL_STG6';
                EXECUTE IMMEDIATE 'TRUNCATE TABLE EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG6';

INSERT /*+PARALLEL(CEB,8)+*/ INTO EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG6 CEB(
BILL_ACCNT_NUM,
MARKET_FLAG                               ,   
--DATE_OF_CONTRACT_START                    ,
STATUS_OF_BILLING_ACCOUNT                 ,
CANCELLATION_DIRECT_DEBIT                 ,
DATE_OF_DIRECT_DEBIT_CANC       ,
COUNT_COMPLAINTS_3MONTHS                  ,
COUNT__OPEN_COMPLAINTS    ,
COUNT_FAULTS_3MONTHS                      ,
COUNT_ENQUIRIES_3MONTHS                   ,
HAS_AOT_IN_PROGRESS                       ,
DATE_OF_OLDEST_BB_SERVICE                 ,
COUNT_OF_CONTACTS_1M                      ,
COUNT_OF_CONTACTS_6M                      ,
IS_CUSTOMER_TENURE                        ,
COUNT_COMPLAINTS_CURRENT_MONTH            ,
COUNT_BT_VISIT_CURRENT_MONTH        ,
DATE_OF_BT_WIFI_LAST_USE                  ,
HAS_BT_WIFI_ACTIVATED                     ,
TOTAL_BT_WIFI_USAGE_3MONTHS               ,
IS_SUB_15_FIBRE_ELIGIBLE                  ,
IS_TV_UHD_ELIGIBLE                        ,
IS_GFAST_100_ELIGIBLE                     ,
IS_GFAST_200_ELIGIBLE                     ,
DATE_OF_FIBRE_ELIGIBILE_START         ,
DATE_OF_FIBRE_RFS_INDICATIVE              ,
IS_BB_FRESHER                             ,
IS_BT_TV_FRESHER                          ,
AVERAGE_MOBILE_MINS_LAST_12MTH            ,
AVERAGE_LOCAL_MINS_LAST_12MTH             ,
IS_ELIGIBLE_FOR_FTTP                      ,
IS_MULTICAST_ON_FIBRE_ELIGIBLE            ,
IS_MULTICAST_ON_COPER_ELIGIBLE         ,
IS_ULTRAFAST_FTTP_ELIGIBLE                ,
IS_SUPER_FAST_FIBRE_ELIGIBLE              ,
HAS_BT_MAILBOX                            ,
HAS_CREDIT_STATUS                         ,
IS_MOBILE_4G_ELIGIBLE                     ,
HAS_BT_CLOUD                              ,
IS_MY_ANYTIME_CAL_PLN_ELIGIBLE        ,
IS_ELIGIBLE_ADSL                          ,
IS_ELIGIBLE_ADSL2                         ,
HAS_VIEWED_FOOTBALL                       ,
HAS_VIEWED_CRICKET                        ,
HAS_VIEWED_BOXING                         ,
HAS_VIEWED_RUGBY                          ,
HAS_VIEWED_SPORT                          ,
HAS_VIEWED_MOTORSPORT                     ,
HAS_VIEWED_AMC                            ,
COUNT_BT_SPORT_VIEWS_3M_APP               ,
COUNT_BT_SPORT_VIEWS_3M                   ,
HAS_VIRUS_PROTECT                         ,
HAS_PARENTAL_CONTROL                      ,
DATE_OF_PARENTAL_ACTIVATION     ,
TOTAL_MINUTES_CURRENT_MONTH               ,
TOTAL_MINUTES_LAST_3_MONTHS               ,
TOTAL_INMINUTE_LAST_3_MONTH   ,
HAS_BROADBAND_HUB                   ,
HAS_BT_SPORT_BT_TV_HD               ,
HAS_BT_SPORT_SKY_HD                 ,
DATE_OF_OLDEST_PSTN_SERVICE         ,
HAS_TV_SKY_SPORTS                   ,
EXISTENCE_PREVIOUS_BROADBAND        ,
HAS_MULTIPLE_BILLING_ACCOUNTS       ,
IS_PRIMARY_BILLING_ACOUNT           ,
IS_CUSTOMER_ORGANISATION            ,
DATE_OF_ACCOUNT_START               ,
IS_ACCOUNT_TYPE                     ,
HAS_DEBT_STATUS                     ,
HAS_BTTV                            ,
IS_BT_PREMIUM_MAIL_STATUS           ,
HAS_OPEN_ORDER_FOR_BROADBAND        ,
HAS_OPEN_ORDER_FOR_BTTV             ,
HAS_OPEN_ORDER_FOR_BT_SPORT         ,
HAS_CALL_BARRING                    ,
HAS_CALL_PROTECT                    ,
HAS_NOW_TV                          ,
DATE_OF_OLDEST_BT_TV_SERVICE        ,
DATE_OF_OLDEST_BT_MOB_SRVICE    ,
DATE_OF_OLDEST_BT_SPRT_SRVIC     ,
HAS_BT_TV_APP                       ,
DATE_OF_DIRECT_DEBIT_START          ,
IS_BT_SPORT_PLATFORM                ,
HAS_FILM_TV_BOX_SET                 ,
HAS_TV_MUSIC_BOLT_ON                ,
HAS_TV_KIDS_BOLT_ON                 ,
HAS_TV_KIDS                         ,
HAS_TV_KIDS_EXTRA                   ,
HAS_SMART_HUB                       ,
HAS_SMART_HUB_2                     ,
HAS_HD_EXTRA                        ,
HAS_BT_SPORT_HD                     ,
BT_SPORT_SECOND_BOX,
HAS_BUNDLE,
HAS_LINE_RENTAL_SAVER,
IS_PSTN_CNTRCT_END,
--DATE_LINE_RTNL_SAVER_CNTCT_END,
--DATE_OF_CONTRACT_END,
IS_TV_PROMOTION,
IS_PSTN_CALLING_PLAN,
IS_PSTN_CALLING_PLAN_OFFER,
DATE_OF_PSTN_OFFER_END,
DATE_OF_PSTN_OFFER_START,
IS_BB_TENURE,
IS_PSTN_TENURE,
IS_BROADBAND_PROMOTION,
BROADBAND_SERVICE_LINE_TYPE,
DATE_MSS_PROMOTION_START,
DATE_MSS_PROMOTION_END,
DATE_BT_TV_CONTRACT_START,
DATE_BT_TV_DISCOUNT_END,
IS_BT_SPORT_PROMOTION,
HAS_BT_SPORT_ON_BT_TV_UHD,
BT_MOBILE_FAMILY_SIM_PROMOTION,
--DATE_OF_DISCOUNT_START,
DATE_OF_DISCOUNT_END,
DATE_VOICE_PROM_DISCOUNT_END,
--HAS_DYNAMIC_DISCOUNT,
--IS_DYNAMIC_DISCOUNT_VALUE,
HAS_MOBILE_ROAMING_AVAILABLE,
IS_BT_MOBILE_CALL_PLAN,
HAS_UNLIMITED_BB_PACKAGE,
--IS_SERVICE_SUB_TYPE,
--HAS_EARLY_LIFE_FLAG,
--IS_CONTRACT_TYPE,
--DATE_OF_SERVICE_START,
--DATE_OF_SERVICE_END,
HAS_BT_TV_MOCO,
HAS_BT_TV_MULTICAST,
IS_BT_BROADBAND_PROMOTION,
IS_LANDLINE_PROMOTION,
IS_BT_TV_PROMOTION,
IS_BT_MOBILE_PROMOTION,
--IS_BUNDLE_PROMOTION,
HAS_BT_TV_HD,
HAS_TPS_FLAG,
DATE_OF_TV_VOD,
IS_BB_AVERAGE_DOWNLOAD_SPEED,
IS_BT_TV_SERVICE_TYPE,
IS_BT_TV_STATUS,
HAS_BT_MOBILE_APP,
HAS_BT_TV_KIDS_USAGE,
HAS_BT_TV_SPORT_USAGE,
HAS_BT_TV_ENTERTAINMENT_USAGE,
HAS_BT_TV_VOD_USAGE,
BT_TV_DEVICE_TYPE,
IS_BT_TV_DEVICE_MODEL,
IS_BT_TV_DEVICE_MODEL_NAME,
IS_BT_TV_CONNECTION_STATUS,
HAS_NETFLIX,
IS_NETFLIX_PACKAGE,
TOTAL_AVERAGE_DAY_REVENUE_P1       ,
TOTAL_AVERAGE_DAY_REVENUE_F        ,
AVERAGE_LOCAL_NON_GEO_MIN          ,
AVERAGE_NATIONAL_GEO_MINS_L        ,
AVERAGE_NATIONAL_NON_GEO_MI        ,
AVERAGE_TOTAL_CALLS_LAST_12MTH     ,
AVERAGE_TOTAL_CALL_MINS_LAS        ,
AVERAGE_DAY_MINS_12MTHS            ,
TOTAL_EVE_MINUTES_LAST_3_MO        ,
TOTAL_EVE_WKND_MINUTES_LAST        ,
AVERAGE_EVE_CALLS_12MTHS           ,
AVERAGE_EVWKND_MINS_12MTHS         ,
AVERAGE_INTL_MINS_LAST_12MTH       ,
AVERAGE_INTL_MOBILE_MINS_LA        ,
AVERAGE_LOCAL_GEO_MINS_LAST        ,
--AVERAGE_MOBILE_MINS_LAST_12MTH
--AVERAGE_FIXED_TO_MOBILE_MIN        ,
TOTAL_UK__MOBILE_MINS_LAST         ,
TOTAL_LOCAL_GEO_MINS_LAST_3MTH     ,
TOTAL_LOCAL_NON_GEO_MINS_L         ,
TOTAL_WEEKEND_MINS_LAST_3MTH       ,
TOTAL_NATIONAL_NON_GEO_MIN         ,
TOTAL_NATIONAL_GEO_MINS_LA         ,
TOTAL_INTERNATIONAL_FREEDO         ,
TOTAL_INTERNATIONAL_ROW_MI         ,
TOTAL_INTERNATIONAL__MOBIL         ,
TOTAL_DAY_TIME_MINS_LAST_3MTHS     
)
(SELECT /*+parallel(WSTG1,8) parallel(WSTG2,8) parallel(WSTG3,8) parallel(WSTG4,8) parallel(WSTG5,8)+*/ distinct
WSTG1.BILL_ACCNT_NUM,
WSTG1.MARKET_FLAG                               ,   
--WSTG1.DATE_OF_CONTRACT_START                  ,
WSTG1.STATUS_OF_BILLING_ACCOUNT                 ,
WSTG1.CANCELLATION_DIRECT_DEBIT                 ,
WSTG1.DATE_OF_DIRECT_DEBIT_CANC                 ,
WSTG1.COUNT_COMPLAINTS_3MONTHS                  ,
WSTG1.COUNT__OPEN_COMPLAINTS                    ,
WSTG1.COUNT_FAULTS_3MONTHS                      ,
WSTG1.COUNT_ENQUIRIES_3MONTHS                   ,        
NVL(WSTG1.HAS_AOT_IN_PROGRESS,'False')                       HAS_AOT_IN_PROGRESS            ,                    
WSTG1.DATE_OF_OLDEST_BB_SERVICE                 ,
WSTG1.COUNT_OF_CONTACTS_1M                      ,
WSTG1.COUNT_OF_CONTACTS_6M                      ,       
NVL(WSTG1.IS_CUSTOMER_TENURE,'False')                        IS_CUSTOMER_TENURE             , 
WSTG1.COUNT_COMPLAINTS_CURRENT_MONTH            ,
WSTG1.COUNT_BT_VISIT_CURRENT_MONTH              ,
WSTG1.DATE_OF_BT_WIFI_LAST_USE                  ,                      
NVL(WSTG1.HAS_BT_WIFI_ACTIVATED,'False')                     HAS_BT_WIFI_ACTIVATED          , 
WSTG1.TOTAL_BT_WIFI_USAGE_3MONTHS               ,                     
NVL(WSTG1.IS_SUB_15_FIBRE_ELIGIBLE,'False')                  IS_SUB_15_FIBRE_ELIGIBLE       ,        
NVL(WSTG1.IS_TV_UHD_ELIGIBLE,'False')                        IS_TV_UHD_ELIGIBLE             ,        
NVL(WSTG1.IS_GFAST_100_ELIGIBLE,'False')                     IS_GFAST_100_ELIGIBLE          ,        
NVL(WSTG1.IS_GFAST_200_ELIGIBLE,'False')                     IS_GFAST_200_ELIGIBLE          , 
WSTG1.DATE_OF_FIBRE_ELIGIBILE_START             ,
WSTG1.DATE_OF_FIBRE_RFS_INDICATIVE              ,                  
NVL(WSTG1.IS_BB_FRESHER,'False')                             IS_BB_FRESHER                  ,        
NVL(WSTG1.IS_BT_TV_FRESHER,'False')                          IS_BT_TV_FRESHER               ,
WSTG1.AVERAGE_MOBILE_MINS_LAST_12MTH            ,
WSTG1.AVERAGE_LOCAL_MINS_LAST_12MTH             ,                    
NVL(WSTG1.IS_ELIGIBLE_FOR_FTTP,'False')                      IS_ELIGIBLE_FOR_FTTP           ,        
NVL(WSTG1.IS_MULTICAST_ON_FIBRE_ELIGIBLE,'False')            IS_MULTICAST_ON_FIBRE_ELIGIBLE ,        
NVL(WSTG1.IS_MULTICAST_ON_COPER_ELIGIBLE,'False')            IS_MULTICAST_ON_COPER_ELIGIBLE ,     
NVL(WSTG1.IS_ULTRAFAST_FTTP_ELIGIBLE,'False')                IS_ULTRAFAST_FTTP_ELIGIBLE     ,        
NVL(WSTG1.IS_SUPER_FAST_FIBRE_ELIGIBLE,'False')              IS_SUPER_FAST_FIBRE_ELIGIBLE   ,        
NVL(WSTG1.HAS_BT_MAILBOX,'False')                            HAS_BT_MAILBOX                 ,        
NVL(WSTG1.HAS_CREDIT_STATUS,'False')                         HAS_CREDIT_STATUS              ,        
NVL(WSTG1.IS_MOBILE_4G_ELIGIBLE,'False')                     IS_MOBILE_4G_ELIGIBLE          ,        
NVL(WSTG1.HAS_BT_CLOUD,'False')                              HAS_BT_CLOUD                   ,        
NVL(WSTG1.IS_MY_ANYTIME_CAL_PLN_ELIGIBLE,'False')            IS_MY_ANYTIME_CAL_PLN_ELIGIBLE ,    
NVL(WSTG1.IS_ELIGIBLE_ADSL,'False')                          IS_ELIGIBLE_ADSL               ,        
NVL(WSTG1.IS_ELIGIBLE_ADSL2,'False')                         IS_ELIGIBLE_ADSL2              ,        
NVL(WSTG1.HAS_VIEWED_FOOTBALL,'False')                       HAS_VIEWED_FOOTBALL            ,       
NVL(WSTG1.HAS_VIEWED_CRICKET,'False')                        HAS_VIEWED_CRICKET             ,        
NVL(WSTG1.HAS_VIEWED_BOXING,'False')                         HAS_VIEWED_BOXING              ,        
NVL(WSTG1.HAS_VIEWED_RUGBY,'False')                          HAS_VIEWED_RUGBY               ,        
NVL(WSTG1.HAS_VIEWED_SPORT,'False')                          HAS_VIEWED_SPORT               ,        
NVL(WSTG1.HAS_VIEWED_MOTORSPORT,'False')                     HAS_VIEWED_MOTORSPORT          ,        
NVL(WSTG1.HAS_VIEWED_AMC,'False')                            HAS_VIEWED_AMC                 ,  
WSTG1.COUNT_BT_SPORT_VIEWS_3M_APP               ,
WSTG1.COUNT_BT_SPORT_VIEWS_3M                   ,                        
NVL(WSTG1.HAS_VIRUS_PROTECT,'False')                         HAS_VIRUS_PROTECT              ,        
NVL(WSTG1.HAS_PARENTAL_CONTROL,'False')                      HAS_PARENTAL_CONTROL           ,
WSTG1.DATE_OF_PARENTAL_ACTIVATION               ,
WSTG1.TOTAL_MINUTES_CURRENT_MONTH               ,
WSTG1.TOTAL_MINUTES_LAST_3_MONTHS               ,
WSTG1.TOTAL_INMINUTE_LAST_3_MONTH               ,
NVL(WSTG2.HAS_BROADBAND_HUB,'False')                         HAS_BROADBAND_HUB            ,    
NVL(WSTG2.HAS_BT_SPORT_BT_TV_HD,'False')                     HAS_BT_SPORT_BT_TV_HD        ,    
NVL(WSTG2.HAS_BT_SPORT_SKY_HD,'False')                       HAS_BT_SPORT_SKY_HD          ,   
WSTG2.DATE_OF_OLDEST_PSTN_SERVICE         ,       
NVL(WSTG2.HAS_TV_SKY_SPORTS,'False')                         HAS_TV_SKY_SPORTS            ,  
WSTG2.EXISTENCE_PREVIOUS_BROADBAND        ,        
NVL(WSTG2.HAS_MULTIPLE_BILLING_ACCOUNTS,'False')             HAS_MULTIPLE_BILLING_ACCOUNTS,    
NVL(WSTG2.IS_PRIMARY_BILLING_ACOUNT,'False')                 IS_PRIMARY_BILLING_ACOUNT    ,    
NVL(WSTG2.IS_CUSTOMER_ORGANISATION,'False')                  IS_CUSTOMER_ORGANISATION     ,   
WSTG2.DATE_OF_ACCOUNT_START               ,             
NVL(WSTG2.IS_ACCOUNT_TYPE,'False')                           IS_ACCOUNT_TYPE              ,    
NVL(WSTG2.HAS_DEBT_STATUS,'False')                           HAS_DEBT_STATUS              ,    
NVL(WSTG2.HAS_BTTV,'False')                                  HAS_BTTV                     ,    
NVL(WSTG2.IS_BT_PREMIUM_MAIL_STATUS,'False')                 IS_BT_PREMIUM_MAIL_STATUS    ,    
NVL(WSTG2.HAS_OPEN_ORDER_FOR_BROADBAND,'False')              HAS_OPEN_ORDER_FOR_BROADBAND ,    
NVL(WSTG2.HAS_OPEN_ORDER_FOR_BTTV,'False')                   HAS_OPEN_ORDER_FOR_BTTV      ,    
NVL(WSTG2.HAS_OPEN_ORDER_FOR_BT_SPORT,'False')               HAS_OPEN_ORDER_FOR_BT_SPORT  ,    
NVL(WSTG2.HAS_CALL_BARRING,'False')                          HAS_CALL_BARRING             ,    
NVL(WSTG2.HAS_CALL_PROTECT,'False')                          HAS_CALL_PROTECT             ,    
NVL(WSTG2.HAS_NOW_TV,'False')                                HAS_NOW_TV                   ,
WSTG2.DATE_OF_OLDEST_BT_TV_SERVICE        ,
WSTG2.DATE_OF_OLDEST_BT_MOB_SRVICE        ,
WSTG2.DATE_OF_OLDEST_BT_SPRT_SRVIC        ,         
NVL(WSTG2.HAS_BT_TV_APP,'False')                             HAS_BT_TV_APP                ,   
WSTG2.DATE_OF_DIRECT_DEBIT_START          ,       
NVL(WSTG2.IS_BT_SPORT_PLATFORM,'False')                      IS_BT_SPORT_PLATFORM         ,    
NVL(WSTG2.HAS_FILM_TV_BOX_SET,'False')                       HAS_FILM_TV_BOX_SET          ,    
NVL(WSTG2.HAS_TV_MUSIC_BOLT_ON,'False')                      HAS_TV_MUSIC_BOLT_ON         ,    
NVL(WSTG2.HAS_TV_KIDS_BOLT_ON,'False')                       HAS_TV_KIDS_BOLT_ON          ,    
NVL(WSTG2.HAS_TV_KIDS,'False')                               HAS_TV_KIDS                  ,    
NVL(WSTG2.HAS_TV_KIDS_EXTRA,'False')                         HAS_TV_KIDS_EXTRA            ,    
NVL(WSTG2.HAS_SMART_HUB,'False')                             HAS_SMART_HUB                ,    
NVL(WSTG2.HAS_SMART_HUB_2,'False')                           HAS_SMART_HUB_2              ,    
NVL(WSTG2.HAS_HD_EXTRA,'False')                              HAS_HD_EXTRA                 ,    
NVL(WSTG2.HAS_BT_SPORT_HD,'False')                           HAS_BT_SPORT_HD              ,
WSTG2.BT_SPORT_SECOND_BOX,
NVL(WSTG3.HAS_BUNDLE,'False')                                HAS_BUNDLE                     ,
NVL(WSTG3.HAS_LINE_RENTAL_SAVER,'False')                     HAS_LINE_RENTAL_SAVER          ,
NVL(WSTG3.IS_PSTN_CNTRCT_END,'False')                        IS_PSTN_CNTRCT_END             ,
--WSTG3.DATE_LINE_RTNL_SAVER_CNTCT_END,
--WSTG3.DATE_OF_CONTRACT_END,
NVL(WSTG3.IS_TV_PROMOTION,'False')                           IS_TV_PROMOTION                ,
NVL(WSTG3.IS_PSTN_CALLING_PLAN,'False')                      IS_PSTN_CALLING_PLAN           ,
NVL(WSTG3.IS_PSTN_CALLING_PLAN_OFFER,'False')                IS_PSTN_CALLING_PLAN_OFFER     ,
WSTG3.DATE_OF_PSTN_OFFER_END,
WSTG3.DATE_OF_PSTN_OFFER_START,
NVL(WSTG3.IS_BB_TENURE,'False')                              IS_BB_TENURE                   ,
NVL(WSTG3.IS_PSTN_TENURE,'False')                            IS_PSTN_TENURE                 ,
NVL(WSTG3.IS_BROADBAND_PROMOTION,'False')                    IS_BROADBAND_PROMOTION         ,
WSTG3.BROADBAND_SERVICE_LINE_TYPE,
WSTG3.DATE_MSS_PROMOTION_START,
WSTG3.DATE_MSS_PROMOTION_END,
WSTG3.DATE_BT_TV_CONTRACT_START,
WSTG3.DATE_BT_TV_DISCOUNT_END,
NVL(WSTG3.IS_BT_SPORT_PROMOTION,'False')                     IS_BT_SPORT_PROMOTION          ,
NVL(WSTG3.HAS_BT_SPORT_ON_BT_TV_UHD,'False')                 HAS_BT_SPORT_ON_BT_TV_UHD      ,
WSTG3.BT_MOBILE_FAMILY_SIM_PROMOTION,
--WSTG3.DATE_OF_DISCOUNT_START,
WSTG3.DATE_OF_DISCOUNT_END,
WSTG3.DATE_VOICE_PROM_DISCOUNT_END,
--NVL(WSTG3.HAS_DYNAMIC_DISCOUNT,'False')                    HAS_DYNAMIC_DISCOUNT           ,
--NVL(WSTG3.IS_DYNAMIC_DISCOUNT_VALUE,'False')               IS_DYNAMIC_DISCOUNT_VALUE      ,
NVL(WSTG3.HAS_MOBILE_ROAMING_AVAILABLE,'False')              HAS_MOBILE_ROAMING_AVAILABLE   ,
NVL(WSTG3.IS_BT_MOBILE_CALL_PLAN,'False')                    IS_BT_MOBILE_CALL_PLAN         ,
NVL(WSTG3.HAS_UNLIMITED_BB_PACKAGE,'False')                  HAS_UNLIMITED_BB_PACKAGE       ,
--NVL(WSTG3.IS_SERVICE_SUB_TYPE,'False')                     IS_SERVICE_SUB_TYPE            ,
--NVL(WSTG3.HAS_EARLY_LIFE_FLAG,'False')                     HAS_EARLY_LIFE_FLAG            ,
--NVL(WSTG3.IS_CONTRACT_TYPE,'False')                        IS_CONTRACT_TYPE               ,
--WSTG3.DATE_OF_SERVICE_START,
--WSTG3.DATE_OF_SERVICE_END,
NVL(WSTG3.HAS_BT_TV_MOCO,'False')                            HAS_BT_TV_MOCO                 ,
NVL(WSTG3.HAS_BT_TV_MULTICAST,'False')                       HAS_BT_TV_MULTICAST            ,
NVL(WSTG3.IS_BT_BROADBAND_PROMOTION,'False')                 IS_BT_BROADBAND_PROMOTION      ,
NVL(WSTG3.IS_LANDLINE_PROMOTION,'False')                     IS_LANDLINE_PROMOTION          ,
NVL(WSTG3.IS_BT_TV_PROMOTION,'False')                        IS_BT_TV_PROMOTION             ,
NVL(WSTG3.IS_BT_MOBILE_PROMOTION,'False')                    IS_BT_MOBILE_PROMOTION         ,
--NVL(WSTG3.IS_BUNDLE_PROMOTION,'False')                     IS_BUNDLE_PROMOTION            ,
NVL(WSTG3.HAS_BT_TV_HD,'False')                              HAS_BT_TV_HD                   ,
NVL(WSTG4.HAS_TPS_FLAG,'False')                              HAS_TPS_FLAG                   ,
WSTG4.DATE_OF_TV_VOD,
NVL(WSTG4.IS_BB_AVERAGE_DOWNLOAD_SPEED,'False')                 IS_BB_AVERAGE_DOWNLOAD_SPEED   ,
NVL(WSTG4.IS_BT_TV_SERVICE_TYPE,'False')                     IS_BT_TV_SERVICE_TYPE          ,
NVL(WSTG4.IS_BT_TV_STATUS,'False')                           IS_BT_TV_STATUS                ,
NVL(WSTG4.HAS_BT_MOBILE_APP,'False')                         HAS_BT_MOBILE_APP              ,
NVL(WSTG4.HAS_BT_TV_KIDS_USAGE,'False')                      HAS_BT_TV_KIDS_USAGE           ,
NVL(WSTG4.HAS_BT_TV_SPORT_USAGE,'False')                     HAS_BT_TV_SPORT_USAGE          ,
NVL(WSTG4.HAS_BT_TV_ENTERTAINMENT_USAGE,'False')             HAS_BT_TV_ENTERTAINMENT_USAGE  ,
NVL(WSTG4.HAS_BT_TV_VOD_USAGE,'False')                       HAS_BT_TV_VOD_USAGE            ,
WSTG4.BT_TV_DEVICE_TYPE,
NVL(WSTG4.IS_BT_TV_DEVICE_MODEL,'False')                     IS_BT_TV_DEVICE_MODEL          ,
NVL(WSTG4.IS_BT_TV_DEVICE_MODEL_NAME,'False')                IS_BT_TV_DEVICE_MODEL_NAME     ,
NVL(WSTG4.IS_BT_TV_CONNECTION_STATUS,'False')                IS_BT_TV_CONNECTION_STATUS     ,
NVL(WSTG4.HAS_NETFLIX,'False')                               HAS_NETFLIX                    ,
NVL(WSTG4.IS_NETFLIX_PACKAGE,'False')                        IS_NETFLIX_PACKAGE        ,
WSTG5.TOTAL_AVERAGE_DAY_REVENUE_P1       ,
WSTG5.TOTAL_AVERAGE_DAY_REVENUE_F        ,
WSTG5.AVERAGE_LOCAL_NON_GEO_MIN          ,
WSTG5.AVERAGE_NATIONAL_GEO_MINS_L        ,
WSTG5.AVERAGE_NATIONAL_NON_GEO_MI        ,
WSTG5.AVERAGE_TOTAL_CALLS_LAST_12MTH     ,
WSTG5.AVERAGE_TOTAL_CALL_MINS_LAS        ,
WSTG5.AVERAGE_DAY_MINS_12MTHS            ,
WSTG5.TOTAL_EVE_MINUTES_LAST_3_MO        ,
WSTG5.TOTAL_EVE_WKND_MINUTES_LAST        ,
WSTG5.AVERAGE_EVE_CALLS_12MTHS           ,
WSTG5.AVERAGE_EVWKND_MINS_12MTHS         ,
WSTG5.AVERAGE_INTL_MINS_LAST_12MTH       ,
WSTG5.AVERAGE_INTL_MOBILE_MINS_LA        ,
WSTG5.AVERAGE_LOCAL_GEO_MINS_LAST        ,
--WSTG5.AVERAGE_MOBILE_MINS_LAST_12MTH     ,
--WSTG5.AVERAGE_FIXED_TO_MOBILE_MIN        ,
WSTG5.TOTAL_UK__MOBILE_MINS_LAST         ,
WSTG5.TOTAL_LOCAL_GEO_MINS_LAST_3MTH     ,
WSTG5.TOTAL_LOCAL_NON_GEO_MINS_L         ,
WSTG5.TOTAL_WEEKEND_MINS_LAST_3MTH       ,
WSTG5.TOTAL_NATIONAL_NON_GEO_MIN         ,
WSTG5.TOTAL_NATIONAL_GEO_MINS_LA         ,
WSTG5.TOTAL_INTERNATIONAL_FREEDO         ,
WSTG5.TOTAL_INTERNATIONAL_ROW_MI         ,
WSTG5.TOTAL_INTERNATIONAL__MOBIL         ,
WSTG5.TOTAL_DAY_TIME_MINS_LAST_3MTHS     

FROM EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG1 WSTG1,
EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG2 WSTG2,
EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG3 WSTG3,
EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG4 WSTG4,
EDW_LOAD_STAGE.CUSTOMER_EDW_BRITE_BILL_STG5 WSTG5,
CUSTOMER_AGGREGATED_CACHE.CUSTOMER_EDW_BRITE_BILL_FACT CEB
WHERE CEB.BILL_ACCNT_NUM=WSTG1.BILL_ACCNT_NUM(+)
  AND CEB.BILL_ACCNT_NUM=WSTG2.BILL_ACCNT_NUM(+)
  AND CEB.BILL_ACCNT_NUM=WSTG3.BILL_ACCNT_NUM(+)
  AND CEB.BILL_ACCNT_NUM=WSTG4.BILL_ACCNT_NUM(+)--SUBSTR(WSTG4.BILL_ACCNT_NUM,-10,10)(+)
  AND CEB.BILL_ACCNT_NUM=WSTG5.BILL_ACCNT_NUM(+));

                                                                                                                                                  
    V_ROW_INSERTED                            := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;  
  ---END  BTR-115327/BTRCE-216867/DSS-35180/RETAILEDW-24103/EDW - Customer Fact creation and enablement on current feed to BB
  
  PROCEDURE PR_W_CUST_EDW_BRITE_BILL_FACT(
      P_RETURN_CODE OUT NUMBER)
  IS
  BEGIN
    V_START_DATE := SYSDATE;
    V_TABLE_NAME := 'W_CUST_EDW_BRITE_BILL_FACT';
    EXECUTE IMMEDIATE 'TRUNCATE TABLE W_CUST_EDW_BRITE_BILL_FACT';
    INSERT INTO W_CUST_EDW_BRITE_BILL_FACT
      (BILL_ACCNT_NUM,FACT_NAME,FACT_VALUE
      )
    SELECT BILL_ACCNT_NUM,
      FACT_NAME,
      FACT_VALUE
    FROM(SELECT CASE
        WHEN SUBSTR(BILL_ACCNT_NUM,1,2)='02'
        THEN 'GB'
          ||SUBSTR(BILL_ACCNT_NUM,3)
        ELSE BILL_ACCNT_NUM
      END  BILL_ACCNT_NUM,
        IS_CONSENT_TO_MARKET ,
        HAS_REALISED_BT_PLUS_BENEFIT ,
        HAS_DIS_END_IN_NEXT_BILL_PRD ,
        --IS_PAPER_ONLY                               ,
        IS_FIBRE_ELIGIBLE ,
        --IS_INFINITY                                 ,
        IS_TV_ELIGIBLE ,
        --IS_ALTERNATE_MEDIA                          ,
        COUNT_OPEN_FAULT ,
        --HAS_BT_APP_BEEN_DOWNLOADED                  ,
        HAS_SPORT_ON_APP ,
        HAS_SPORT_ON_APP_ONLY ,
        HAS_TV_STARTER_PLAN ,
        HAS_TV_ENTERTAINMENT_PLAN ,
        HAS_TV_MAX_PLAN ,
        HAS_BT_MOBILE_SIM_ONLY ,
        HAS_BT_MOBILE_FAMILY_SIM ,
        HAS_BT_MOBILE_SMARTPHONE ,
        HAS_CALLER_DISPLAY ,
        IS_BT_TV ,
        --BILL_MEDIA                                  ,
        IS_BT_EMPLOYEE ,
        TO_CHAR(DATE_OF_LAST_LOGIN,'YYYYMMDD') DATE_OF_LAST_LOGIN ,
        FOLLOW_UP_RATING ,
        HAS_OPEN_COMPLAINT ,
        HAS_GIFTED_EMPLOYEE_BB ,
        HAS_COPPER_BROADBAND ,
        IS_CARRIER_PRE_SELECT ,
        HAS_BT_SPORT_PAID_FOR ,
        HAS_BT_ID_BEEN_ACTIVATED ,
        HAS_ULTRA_FAST_FIBRE_2_PLUS ,
        HAS_ULTRA_FAST_FIBRE_PLUS ,
        HAS_SUPER_FAST_FIBRE_2_UNL ,
        HAS_SUPER_FAST_FIBRE_1 ,
        HAS_SUPER_FAST_FIBRE_LITE ,
        HAS_BROADBAND_UNLIMITED ,
        HAS_BROADBAND_PACKAGE ,
        HAS_MOBILE_ROAMING_RESTRICTION ,
        HAS_SPORT_ON_TV_FIBRE ,
        HAS_SPORT_ON_TV_COPPER,
       to_char(BB_CONTRACT_END_DATE,'YYYYMMDD') BB_CONTRACT_END_DATE,
to_char(TV_CONTRACT_END_DATE,'YYYYMMDD' ) TV_CONTRACT_END_DATE,
to_char(MOBILE_CONTRACT_END_DATE,'YYYYMMDD' ) MOBILE_CONTRACT_END_DATE,
IS_IN_BB_RCW,
IS_IN_TV_RCW,
IS_IN_MOBILE_RCW,
----START BTR-115327/BTRCE-216867/DSS-35180/RETAILEDW-24103/EDW - Customer Fact creation and enablement on current feed to BB
MARKET_FLAG                               ,   
--DATE_OF_CONTRACT_START                    ,
STATUS_OF_BILLING_ACCOUNT                 ,
CANCELLATION_DIRECT_DEBIT                 ,
TO_CHAR(DATE_OF_DIRECT_DEBIT_CANC,'YYYYMMDD') DATE_OF_DIRECT_DEBIT_CANC ,
COUNT_COMPLAINTS_3MONTHS                  ,
COUNT__OPEN_COMPLAINTS    ,
COUNT_FAULTS_3MONTHS                      ,
COUNT_ENQUIRIES_3MONTHS                   ,
HAS_AOT_IN_PROGRESS                       ,
TO_CHAR(DATE_OF_OLDEST_BB_SERVICE,'YYYYMMDD') DATE_OF_OLDEST_BB_SERVICE,
COUNT_OF_CONTACTS_1M                      ,
COUNT_OF_CONTACTS_6M                      ,
IS_CUSTOMER_TENURE                        ,
COUNT_COMPLAINTS_CURRENT_MONTH            ,
COUNT_BT_VISIT_CURRENT_MONTH        ,
TO_CHAR(DATE_OF_BT_WIFI_LAST_USE,'YYYYMMDD') DATE_OF_BT_WIFI_LAST_USE,
HAS_BT_WIFI_ACTIVATED                     ,
TOTAL_BT_WIFI_USAGE_3MONTHS               ,
IS_SUB_15_FIBRE_ELIGIBLE                  ,
IS_TV_UHD_ELIGIBLE                        ,
IS_GFAST_100_ELIGIBLE                     ,
IS_GFAST_200_ELIGIBLE                     ,
TO_CHAR(DATE_OF_FIBRE_ELIGIBILE_START ,'YYYYMMDD') DATE_OF_FIBRE_ELIGIBILE_START        ,
TO_CHAR(DATE_OF_FIBRE_RFS_INDICATIVE ,'YYYYMMDD') DATE_OF_FIBRE_RFS_INDICATIVE             ,
IS_BB_FRESHER                             ,
IS_BT_TV_FRESHER                          ,
AVERAGE_MOBILE_MINS_LAST_12MTH            ,
AVERAGE_LOCAL_MINS_LAST_12MTH             ,
IS_ELIGIBLE_FOR_FTTP                      ,
IS_MULTICAST_ON_FIBRE_ELIGIBLE            ,
IS_MULTICAST_ON_COPER_ELIGIBLE         ,
IS_ULTRAFAST_FTTP_ELIGIBLE                ,
IS_SUPER_FAST_FIBRE_ELIGIBLE              ,
HAS_BT_MAILBOX                            ,
HAS_CREDIT_STATUS                         ,
IS_MOBILE_4G_ELIGIBLE                     ,
HAS_BT_CLOUD                              ,
IS_MY_ANYTIME_CAL_PLN_ELIGIBLE        ,
IS_ELIGIBLE_ADSL                          ,
IS_ELIGIBLE_ADSL2                         ,
HAS_VIEWED_FOOTBALL                       ,
HAS_VIEWED_CRICKET                        ,
HAS_VIEWED_BOXING                         ,
HAS_VIEWED_RUGBY                          ,
HAS_VIEWED_SPORT                          ,
HAS_VIEWED_MOTORSPORT                     ,
HAS_VIEWED_AMC                            ,
COUNT_BT_SPORT_VIEWS_3M_APP               ,
COUNT_BT_SPORT_VIEWS_3M                   ,
HAS_VIRUS_PROTECT                         ,
HAS_PARENTAL_CONTROL                      ,
TO_CHAR(DATE_OF_PARENTAL_ACTIVATION,'YYYYMMDD') DATE_OF_PARENTAL_ACTIVATION          ,
TOTAL_MINUTES_CURRENT_MONTH               ,
TOTAL_MINUTES_LAST_3_MONTHS               ,
TOTAL_INMINUTE_LAST_3_MONTH   ,
HAS_BROADBAND_HUB                   ,
HAS_BT_SPORT_BT_TV_HD               ,
HAS_BT_SPORT_SKY_HD                 ,
TO_CHAR(DATE_OF_OLDEST_PSTN_SERVICE,'YYYYMMDD') DATE_OF_OLDEST_PSTN_SERVICE         ,
HAS_TV_SKY_SPORTS                   ,
EXISTENCE_PREVIOUS_BROADBAND        ,
HAS_MULTIPLE_BILLING_ACCOUNTS       ,
IS_PRIMARY_BILLING_ACOUNT           ,
IS_CUSTOMER_ORGANISATION            ,
TO_CHAR(DATE_OF_ACCOUNT_START,'YYYYMMDD') DATE_OF_ACCOUNT_START,'YYYYMMDD'               ,
IS_ACCOUNT_TYPE                     ,
HAS_DEBT_STATUS                     ,
HAS_BTTV                            ,
IS_BT_PREMIUM_MAIL_STATUS           ,
HAS_OPEN_ORDER_FOR_BROADBAND        ,
HAS_OPEN_ORDER_FOR_BTTV             ,
HAS_OPEN_ORDER_FOR_BT_SPORT         ,
HAS_CALL_BARRING                    ,
HAS_CALL_PROTECT                    ,
HAS_NOW_TV                          ,
TO_CHAR(DATE_OF_OLDEST_BT_TV_SERVICE,'YYYYMMDD') DATE_OF_OLDEST_BT_TV_SERVICE        ,
TO_CHAR(DATE_OF_OLDEST_BT_MOB_SRVICE,'YYYYMMDD') DATE_OF_OLDEST_BT_MOB_SRVICE    ,
TO_CHAR(DATE_OF_OLDEST_BT_SPRT_SRVIC,'YYYYMMDD') DATE_OF_OLDEST_BT_SPRT_SRVIC     ,
HAS_BT_TV_APP                       ,
TO_CHAR(DATE_OF_DIRECT_DEBIT_START,'YYYYMMDD') DATE_OF_DIRECT_DEBIT_START          ,
IS_BT_SPORT_PLATFORM                ,
HAS_FILM_TV_BOX_SET                 ,
HAS_TV_MUSIC_BOLT_ON                ,
HAS_TV_KIDS_BOLT_ON                 ,
HAS_TV_KIDS                         ,
HAS_TV_KIDS_EXTRA                   ,
HAS_SMART_HUB                       ,
HAS_SMART_HUB_2                     ,
HAS_HD_EXTRA                        ,
HAS_BT_SPORT_HD                     ,
BT_SPORT_SECOND_BOX,
HAS_BUNDLE,
HAS_LINE_RENTAL_SAVER,
IS_PSTN_CNTRCT_END,
--TO_CHAR(DATE_LINE_RTNL_SAVER_CNTCT_END,
--TO_CHAR(DATE_OF_CONTRACT_END,
IS_TV_PROMOTION,
IS_PSTN_CALLING_PLAN,
IS_PSTN_CALLING_PLAN_OFFER,
TO_CHAR(DATE_OF_PSTN_OFFER_END,'YYYYMMDD') DATE_OF_PSTN_OFFER_END,
TO_CHAR(DATE_OF_PSTN_OFFER_START,'YYYYMMDD') DATE_OF_PSTN_OFFER_START,
IS_BB_TENURE,
IS_PSTN_TENURE,
IS_BROADBAND_PROMOTION,
BROADBAND_SERVICE_LINE_TYPE,
TO_CHAR(DATE_MSS_PROMOTION_START,'YYYYMMDD') DATE_MSS_PROMOTION_START,
TO_CHAR(DATE_MSS_PROMOTION_END,'YYYYMMDD') DATE_MSS_PROMOTION_END,
TO_CHAR(DATE_BT_TV_CONTRACT_START,'YYYYMMDD') DATE_BT_TV_CONTRACT_START,
TO_CHAR(DATE_BT_TV_DISCOUNT_END,'YYYYMMDD') DATE_BT_TV_DISCOUNT_END,
IS_BT_SPORT_PROMOTION,
HAS_BT_SPORT_ON_BT_TV_UHD,
BT_MOBILE_FAMILY_SIM_PROMOTION,
--DATE_OF_DISCOUNT_START,
TO_CHAR(DATE_OF_DISCOUNT_END,'YYYYMMDD') DATE_OF_DISCOUNT_END,
TO_CHAR(DATE_VOICE_PROM_DISCOUNT_END,'YYYYMMDD') DATE_VOICE_PROM_DISCOUNT_END,
--HAS_DYNAMIC_DISCOUNT,
--IS_DYNAMIC_DISCOUNT_VALUE,
HAS_MOBILE_ROAMING_AVAILABLE,
IS_BT_MOBILE_CALL_PLAN,
HAS_UNLIMITED_BB_PACKAGE,
--IS_SERVICE_SUB_TYPE,
--HAS_EARLY_LIFE_FLAG,
--IS_CONTRACT_TYPE,
--DATE_OF_SERVICE_START,
--DATE_OF_SERVICE_END,
HAS_BT_TV_MOCO,
HAS_BT_TV_MULTICAST,
IS_BT_BROADBAND_PROMOTION,
IS_LANDLINE_PROMOTION,
IS_BT_TV_PROMOTION,
IS_BT_MOBILE_PROMOTION,
--IS_BUNDLE_PROMOTION,
HAS_BT_TV_HD,
HAS_TPS_FLAG,
TO_CHAR(DATE_OF_TV_VOD,'YYYYMMDD') DATE_OF_TV_VOD,
IS_BB_AVERAGE_DOWNLOAD_SPEED,
IS_BT_TV_SERVICE_TYPE,
IS_BT_TV_STATUS,
HAS_BT_MOBILE_APP,
HAS_BT_TV_KIDS_USAGE,
HAS_BT_TV_SPORT_USAGE,
HAS_BT_TV_ENTERTAINMENT_USAGE,
HAS_BT_TV_VOD_USAGE,
BT_TV_DEVICE_TYPE,
IS_BT_TV_DEVICE_MODEL,
IS_BT_TV_DEVICE_MODEL_NAME,
IS_BT_TV_CONNECTION_STATUS,
HAS_NETFLIX,
IS_NETFLIX_PACKAGE,
TOTAL_AVERAGE_DAY_REVENUE_P1       ,
TOTAL_AVERAGE_DAY_REVENUE_F        ,
AVERAGE_LOCAL_NON_GEO_MIN          ,
AVERAGE_NATIONAL_GEO_MINS_L        ,
AVERAGE_NATIONAL_NON_GEO_MI        ,
AVERAGE_TOTAL_CALLS_LAST_12MTH     ,
AVERAGE_TOTAL_CALL_MINS_LAS        ,
AVERAGE_DAY_MINS_12MTHS            ,
TOTAL_EVE_MINUTES_LAST_3_MO        ,
TOTAL_EVE_WKND_MINUTES_LAST        ,
AVERAGE_EVE_CALLS_12MTHS           ,
AVERAGE_EVWKND_MINS_12MTHS         ,
AVERAGE_INTL_MINS_LAST_12MTH       ,
AVERAGE_INTL_MOBILE_MINS_LA        ,
AVERAGE_LOCAL_GEO_MINS_LAST        ,
--AVERAGE_MOBILE_MINS_LAST_12MTH
--AVERAGE_FIXED_TO_MOBILE_MIN        ,
TOTAL_UK__MOBILE_MINS_LAST         ,
TOTAL_LOCAL_GEO_MINS_LAST_3MTH     ,
TOTAL_LOCAL_NON_GEO_MINS_L         ,
TOTAL_WEEKEND_MINS_LAST_3MTH       ,
TOTAL_NATIONAL_NON_GEO_MIN         ,
TOTAL_NATIONAL_GEO_MINS_LA         ,
TOTAL_INTERNATIONAL_FREEDO         ,
TOTAL_INTERNATIONAL_ROW_MI         ,
TOTAL_INTERNATIONAL__MOBIL         ,
TOTAL_DAY_TIME_MINS_LAST_3MTHS  
  ----END BTR-115327/BTRCE-216867/DSS-35180/RETAILEDW-24103/EDW - Customer Fact creation and enablement on current feed to BB      
      FROM CUSTOMER_AGGREGATED_CACHE.CUSTOMER_EDW_BRITE_BILL_FACT
      WHERE TRUNC(next_bill_date)                        =TRUNC(sysdate)
      )  UNPIVOT INCLUDE NULLS (FACT_VALUE FOR FACT_NAME IN ( IS_CONSENT_TO_MARKET , HAS_REALISED_BT_PLUS_BENEFIT , HAS_DIS_END_IN_NEXT_BILL_PRD AS 'HAS_DISCOUNT_END_IN_NEXT_BILL_PERIOD' ,
      --IS_PAPER_ONLY                               ,
      IS_FIBRE_ELIGIBLE ,
      --IS_INFINITY                                 ,
      IS_TV_ELIGIBLE ,
      --IS_ALTERNATE_MEDIA                          ,
      COUNT_OPEN_FAULT ,
      --HAS_BT_APP_BEEN_DOWNLOADED                  ,
      HAS_SPORT_ON_APP , HAS_SPORT_ON_APP_ONLY , HAS_TV_STARTER_PLAN , HAS_TV_ENTERTAINMENT_PLAN , HAS_TV_MAX_PLAN , HAS_BT_MOBILE_SIM_ONLY , HAS_BT_MOBILE_FAMILY_SIM , HAS_BT_MOBILE_SMARTPHONE , HAS_CALLER_DISPLAY , IS_BT_TV AS 'IS_BT_TV_ACTIVATION_DATE' ,
      --BILL_MEDIA                                  ,
      IS_BT_EMPLOYEE , DATE_OF_LAST_LOGIN , FOLLOW_UP_RATING , HAS_OPEN_COMPLAINT , HAS_GIFTED_EMPLOYEE_BB , HAS_COPPER_BROADBAND , IS_CARRIER_PRE_SELECT , HAS_BT_SPORT_PAID_FOR , HAS_BT_ID_BEEN_ACTIVATED , HAS_ULTRA_FAST_FIBRE_2_PLUS , HAS_ULTRA_FAST_FIBRE_PLUS , HAS_SUPER_FAST_FIBRE_2_UNL AS 'HAS_SUPER_FAST_FIBRE_2_UNLIMITED' , HAS_SUPER_FAST_FIBRE_1 , HAS_SUPER_FAST_FIBRE_LITE AS 'HAS_SUPER_FAST_FIBRE_LIGHT' , HAS_BROADBAND_UNLIMITED , HAS_BROADBAND_PACKAGE , HAS_MOBILE_ROAMING_RESTRICTION , HAS_SPORT_ON_TV_FIBRE , HAS_SPORT_ON_TV_COPPER
      , BB_CONTRACT_END_DATE ,
TV_CONTRACT_END_DATE ,
MOBILE_CONTRACT_END_DATE ,
IS_IN_BB_RCW,
IS_IN_TV_RCW,
IS_IN_MOBILE_RCW,
---START BTR-115327/BTRCE-216867/DSS-35180/RETAILEDW-24103/EDW - Customer Fact creation and enablement on current feed to BB
MARKET_FLAG                               ,   
--DATE_OF_CONTRACT_START                    ,
STATUS_OF_BILLING_ACCOUNT                 ,
CANCELLATION_DIRECT_DEBIT                 ,
DATE_OF_DIRECT_DEBIT_CANC      AS 'DATE_OF_DIRECT_DEBIT_CANCELLATION' ,
COUNT_COMPLAINTS_3MONTHS                  ,
COUNT__OPEN_COMPLAINTS    AS 'COUNT__OPEN_COMPLAINTS_CURRENT_MONTH' ,
COUNT_FAULTS_3MONTHS                      ,
COUNT_ENQUIRIES_3MONTHS                   ,
HAS_AOT_IN_PROGRESS                       ,
DATE_OF_OLDEST_BB_SERVICE                 ,
COUNT_OF_CONTACTS_1M                      ,
COUNT_OF_CONTACTS_6M                      ,
IS_CUSTOMER_TENURE                        ,
COUNT_COMPLAINTS_CURRENT_MONTH            ,
COUNT_BT_VISIT_CURRENT_MONTH    AS 'COUNT_BTCOM_VISITS_CURRENT_MONTH'     ,
DATE_OF_BT_WIFI_LAST_USE                  ,
HAS_BT_WIFI_ACTIVATED                     ,
TOTAL_BT_WIFI_USAGE_3MONTHS               ,
IS_SUB_15_FIBRE_ELIGIBLE                  ,
IS_TV_UHD_ELIGIBLE                        ,
IS_GFAST_100_ELIGIBLE                     ,
IS_GFAST_200_ELIGIBLE                     ,
DATE_OF_FIBRE_ELIGIBILE_START    AS 'DATE_OF_FIBRE_ELIGIBILITY_START'      ,
DATE_OF_FIBRE_RFS_INDICATIVE              ,
IS_BB_FRESHER                             ,
IS_BT_TV_FRESHER                          ,
AVERAGE_MOBILE_MINS_LAST_12MTH            ,
AVERAGE_LOCAL_MINS_LAST_12MTH             ,
IS_ELIGIBLE_FOR_FTTP                      ,
IS_MULTICAST_ON_FIBRE_ELIGIBLE            ,
IS_MULTICAST_ON_COPER_ELIGIBLE     AS 'IS_MULTICAST_ON_COPPER_ELIGIBLE'     ,
IS_ULTRAFAST_FTTP_ELIGIBLE                ,
IS_SUPER_FAST_FIBRE_ELIGIBLE              ,
HAS_BT_MAILBOX                            ,
HAS_CREDIT_STATUS                         ,
IS_MOBILE_4G_ELIGIBLE                     ,
HAS_BT_CLOUD                              ,
IS_MY_ANYTIME_CAL_PLN_ELIGIBLE       AS 'IS_MY_ANYTIME_CALL_PLAN_ELIGIBLE'  ,
IS_ELIGIBLE_ADSL                          ,
IS_ELIGIBLE_ADSL2                         ,
HAS_VIEWED_FOOTBALL                       ,
HAS_VIEWED_CRICKET                        ,
HAS_VIEWED_BOXING                         ,
HAS_VIEWED_RUGBY                          ,
HAS_VIEWED_SPORT                          ,
HAS_VIEWED_MOTORSPORT                     ,
HAS_VIEWED_AMC                            ,
COUNT_BT_SPORT_VIEWS_3M_APP               ,
COUNT_BT_SPORT_VIEWS_3M                   ,
HAS_VIRUS_PROTECT                         ,
HAS_PARENTAL_CONTROL                      ,
DATE_OF_PARENTAL_ACTIVATION    AS 'DATE_OF_PARENTAL_CONTROL_ACTIVATION'  ,
TOTAL_MINUTES_CURRENT_MONTH               ,
TOTAL_MINUTES_LAST_3_MONTHS               ,
TOTAL_INMINUTE_LAST_3_MONTH   AS 'TOTAL__INTL_MINUTES_LAST_3_MONTHS' ,
HAS_BROADBAND_HUB                   ,
HAS_BT_SPORT_BT_TV_HD               ,
HAS_BT_SPORT_SKY_HD                 ,
DATE_OF_OLDEST_PSTN_SERVICE         ,
HAS_TV_SKY_SPORTS                   ,
EXISTENCE_PREVIOUS_BROADBAND        ,
HAS_MULTIPLE_BILLING_ACCOUNTS       ,
IS_PRIMARY_BILLING_ACOUNT           ,
IS_CUSTOMER_ORGANISATION            ,
DATE_OF_ACCOUNT_START               ,
IS_ACCOUNT_TYPE                     ,
HAS_DEBT_STATUS                     ,
HAS_BTTV                            ,
IS_BT_PREMIUM_MAIL_STATUS           ,
HAS_OPEN_ORDER_FOR_BROADBAND        ,
HAS_OPEN_ORDER_FOR_BTTV             ,
HAS_OPEN_ORDER_FOR_BT_SPORT         ,
HAS_CALL_BARRING                    ,
HAS_CALL_PROTECT                    ,
HAS_NOW_TV                          ,
DATE_OF_OLDEST_BT_TV_SERVICE        ,
DATE_OF_OLDEST_BT_MOB_SRVICE    AS 'DATE_OF_OLDEST_BT_MOBILE_SERVICE' ,
DATE_OF_OLDEST_BT_SPRT_SRVIC    AS 'DATE_OF_OLDEST_BT_SPORT_SERVICE'  ,
HAS_BT_TV_APP                       ,
DATE_OF_DIRECT_DEBIT_START          ,
IS_BT_SPORT_PLATFORM                ,
HAS_FILM_TV_BOX_SET                 ,
HAS_TV_MUSIC_BOLT_ON                ,
HAS_TV_KIDS_BOLT_ON                 ,
HAS_TV_KIDS                         ,
HAS_TV_KIDS_EXTRA                   ,
HAS_SMART_HUB                       ,
HAS_SMART_HUB_2                     ,
HAS_HD_EXTRA                        ,
HAS_BT_SPORT_HD                     ,
BT_SPORT_SECOND_BOX,
HAS_BUNDLE,
HAS_LINE_RENTAL_SAVER,
IS_PSTN_CNTRCT_END  AS 'IS_PSTN_PROMOTION_DAYS_TO_CONTRACT_END',
--DATE_LINE_RTNL_SAVER_CNTCT_END,
--DATE_OF_CONTRACT_END,
IS_TV_PROMOTION,
IS_PSTN_CALLING_PLAN,
IS_PSTN_CALLING_PLAN_OFFER,
DATE_OF_PSTN_OFFER_END,
DATE_OF_PSTN_OFFER_START,
IS_BB_TENURE,
IS_PSTN_TENURE,
IS_BROADBAND_PROMOTION,
BROADBAND_SERVICE_LINE_TYPE,
DATE_MSS_PROMOTION_START,
DATE_MSS_PROMOTION_END,
DATE_BT_TV_CONTRACT_START,
DATE_BT_TV_DISCOUNT_END,
IS_BT_SPORT_PROMOTION,
HAS_BT_SPORT_ON_BT_TV_UHD,
BT_MOBILE_FAMILY_SIM_PROMOTION,
--DATE_OF_DISCOUNT_START,
DATE_OF_DISCOUNT_END,
DATE_VOICE_PROM_DISCOUNT_END,
--HAS_DYNAMIC_DISCOUNT,
--IS_DYNAMIC_DISCOUNT_VALUE,
HAS_MOBILE_ROAMING_AVAILABLE,
IS_BT_MOBILE_CALL_PLAN,
HAS_UNLIMITED_BB_PACKAGE,
--IS_SERVICE_SUB_TYPE,
--HAS_EARLY_LIFE_FLAG,
--IS_CONTRACT_TYPE,
--DATE_OF_SERVICE_START,
--DATE_OF_SERVICE_END,
HAS_BT_TV_MOCO,
HAS_BT_TV_MULTICAST,
IS_BT_BROADBAND_PROMOTION,
IS_LANDLINE_PROMOTION,
IS_BT_TV_PROMOTION,
IS_BT_MOBILE_PROMOTION,
--IS_BUNDLE_PROMOTION,
HAS_BT_TV_HD,
HAS_TPS_FLAG,
DATE_OF_TV_VOD,
IS_BB_AVERAGE_DOWNLOAD_SPEED,
IS_BT_TV_SERVICE_TYPE,
IS_BT_TV_STATUS,
HAS_BT_MOBILE_APP,
HAS_BT_TV_KIDS_USAGE,
HAS_BT_TV_SPORT_USAGE,
HAS_BT_TV_ENTERTAINMENT_USAGE,
HAS_BT_TV_VOD_USAGE,
BT_TV_DEVICE_TYPE,
IS_BT_TV_DEVICE_MODEL,
IS_BT_TV_DEVICE_MODEL_NAME,
IS_BT_TV_CONNECTION_STATUS,
HAS_NETFLIX,
IS_NETFLIX_PACKAGE,
TOTAL_AVERAGE_DAY_REVENUE_P1       ,
TOTAL_AVERAGE_DAY_REVENUE_F     AS 'TOTAL_AVERAGE_DAY_REVENUE_F2M_P1'    ,
AVERAGE_LOCAL_NON_GEO_MIN        AS 'AVERAGE_LOCAL_NON_GEO_MINS_LAST_12MTH'   ,
AVERAGE_NATIONAL_GEO_MINS_L       AS 'AVERAGE_NATIONAL_GEO_MINS_LAST_12MTH'  ,
AVERAGE_NATIONAL_NON_GEO_MI     AS 'AVERAGE_NATIONAL_NON_GEO_MINS_LAST_12MTH'    ,
AVERAGE_TOTAL_CALLS_LAST_12MTH     ,
AVERAGE_TOTAL_CALL_MINS_LAS       AS 'AVERAGE_TOTAL_CALL_MINS_LAST_12MTH' ,
AVERAGE_DAY_MINS_12MTHS            ,
TOTAL_EVE_MINUTES_LAST_3_MO     AS 'TOTAL_EVE_MINUTES_LAST_3_MONTHS'    ,
TOTAL_EVE_WKND_MINUTES_LAST      AS 'TOTAL_EVE_WKND_MINUTES_LAST_3_MONTHS'   ,
AVERAGE_EVE_CALLS_12MTHS           ,
AVERAGE_EVWKND_MINS_12MTHS         ,
AVERAGE_INTL_MINS_LAST_12MTH       ,
AVERAGE_INTL_MOBILE_MINS_LA    AS 'AVERAGE_INTL_MOBILE_MINS_LAST_12MTH'     ,
AVERAGE_LOCAL_GEO_MINS_LAST    AS 'AVERAGE_LOCAL_GEO_MINS_LAST_12MTH'     ,
--AVERAGE_MOBILE_MINS_LAST_12MTH
--AVERAGE_FIXED_TO_MOBILE_MIN      AS 'AVERAGE_FIXED_TO_MOBILE_MINS_LAST_12MTH'   ,
TOTAL_UK__MOBILE_MINS_LAST     AS 'TOTAL_UK__MOBILE_MINS_LAST_3MTH'     ,
TOTAL_LOCAL_GEO_MINS_LAST_3MTH     ,
TOTAL_LOCAL_NON_GEO_MINS_L       AS 'TOTAL_LOCAL_NON_GEO_MINS_LAST_3MTH'   ,
TOTAL_WEEKEND_MINS_LAST_3MTH       ,
TOTAL_NATIONAL_NON_GEO_MIN    AS 'TOTAL_NATIONAL_NON_GEO_MINS_LAST_3MTH'      ,
TOTAL_NATIONAL_GEO_MINS_LA     AS 'TOTAL_NATIONAL_GEO_MINS_LAST_3MTH'     ,
TOTAL_INTERNATIONAL_FREEDO     AS 'TOTAL_INTERNATIONAL_FREEDOM_MINS_LAST_3MTH'     ,
TOTAL_INTERNATIONAL_ROW_MI   AS 'TOTAL_INTERNATIONAL_ROW_MINS_LAST_3MTH'       ,
TOTAL_INTERNATIONAL__MOBIL   AS 'TOTAL_INTERNATIONAL__MOBILE_MINS_LAST_3MTH'       ,
TOTAL_DAY_TIME_MINS_LAST_3MTHS  
---END  BTR-115327/BTRCE-216867/DSS-35180/RETAILEDW-24103/EDW - Customer Fact creation and enablement on current feed to BB
));
    V_ROW_INSERTED := SQL%ROWCOUNT;
    COMMIT;
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_SUCCESS, MESSAGE => CO_SUCCESS_MESSAGE, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    --------------------------------------------
    P_RETURN_CODE := 0;
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    V_ERR_MSG := SUBSTR(SQLCODE || ':' || SQLERRM, 1, 2000);
    PR_LOG_AUDIT(PROCEDURE_NAME => CO_PROCEDURE_NAME, TRG_SCHEMA => CO_TRG_SCHEMA_NAME, TRG_TABLE => V_TABLE_NAME, STATUS => CO_FAILURE, MESSAGE => V_ERR_MSG, ROW_COUNT => V_ROW_INSERTED, PROCESS_START_DATE => V_START_DATE);
    P_RETURN_CODE := 1;
  END;
END;
/
