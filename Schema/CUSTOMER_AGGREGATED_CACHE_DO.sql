  ALTER TABLE CUSTOMER_EDW_BRITE_BILL_FACT ADD 
	(	HAS_BROADBAND_CLG_OFR VARCHAR2(2), 
		HAS_MOBILE_CLG_OFR VARCHAR2(2), 
		HAS_BROADBAND_CLG_LEAVER_OFR VARCHAR2(2), 
		HAS_MOBILE_CLG_LEAVER_OFR VARCHAR2(2),
		CLG_OFR_STRT_LAST_BILL_PRD VARCHAR2(2), 
		CLG_LVR_OFR_STRT_LAST_BILL_PRD VARCHAR2(2), 
		CLG_OFR_BROADBAND_DISC VARCHAR2(10), 
		CLG_OFR_MOBILE_DISC VARCHAR2(10), 
		CLG_OFR_LEAVERS_BB_DISC VARCHAR2(10), 
		CLG_OFR_LEAVERS_MOBILE_DISC VARCHAR2(10)	
	);